*** Settings ***
Resource        ../basesms.robot
Test Setup      Nova sessão     SMS     
Test Teardown   Encerra Sessão

*** Variables ***
${invalid_password}                 Pgmais
${invalid_login}                    Logininvalido
${invalid_login2}                   qapgmais@gmail.com
${login}                            teste@sms.com
${password}                         Pgmais123
${full_name_dashboard}              Teste Sms
${full_name_dashboard_externo}      qaexterno@sms.com
${login_externo}                    qaexterno@sms.com
${password_externo}                 Pgmais123
${title1}                           Esqueceu sua senha ?
${title2}                           Confira Sua caixa de entrada
${title3}                           Bem vindo ao painel PgMais!
${title4}                           O login informado não é um e-mail, favor informar um e-mail

*** Test Cases ***

Cenario 01: Login com senha incorreta (Painel SMS) 
    [Tags]                               Regressao         
    Dado que estou no login unico dos paineis       ${login}                               password=${invalid_password}
    Quando clicar em "entrar"
    Deverá exibir a mensagem de usuário ou senha invalidos


Cenario 02: Login com usuário invalido (Painel SMS)  
    [Tags]                               Regressao         
    Dado que estou no login unico dos paineis       ${login}${invalid_login}                ${password}
    Quando clicar em "entrar"
    Deverá exibir a mensagem de usuário ou senha invalidos
    

Cenario 03: Login com usuário correto até seleção de empresas (Painel SMS)
    [Tags]                               Regressao         
    Dado que estou no login unico dos paineis       ${login}                               ${password}
    Quando clicar em "entrar"
    Deverá ver o usuário logado                     ${full_name}   
    
   
Cenario 05: Login completo (até chegar ao dashboard) (Painel SMS)
    [Tags]                               Regressao         Daily
    Dado que estou no login unico dos paineis       ${login}                               ${password}
    Quando clicar em "entrar"
    Deverá ver o usuário logado                     ${full_name}
    Então deverá selecionar a empresa
    Quando clicar em "confirmar"
    Então o sistema deverá realizar o login e exibir a dashbord do sistema

Cenario 06: Logoff após estar na Dashboard (Painel SMS)
    [Tags]                               Regressao         
    Dado que estou no login unico dos paineis       ${login}                               ${password}
    Quando clicar em "entrar"
    Deverá ver o usuário logado                     ${full_name}
    Então deverá selecionar a empresa
    Quando clicar em "confirmar"
    Então o sistema deverá realizar o login e exibir a dashbord do sistema  
    Quando realizar o logout na dashbord
    Então o sistema devera retornar para a tela principal  


Cenario 07: Solicitar recuperação de senha (Painel SMS)
    [Tags]                              Regressao         
    Quando clicar em esqueci minha senha
    Então devera preencher o email da conta a ser recuperada        ${login}
    Quando clicar em enviar
    Então Devera realizar a recuperação da senha
    #Quando clicar em voltar
    #Então o sistema devera retornar para a tela principal

Cenario 08: Solicitar envio de nova senha informando email invalido (Painel SMS)
    [tags]                              Regressao         
    Quando clicar em esqueci minha senha
    Então devera preencher o email da conta a ser recuperada        ${invalid_login}
    Quando clicar em enviar
    Então deverá ser exibida a mensagem de erro
Cenario 09: Solicitar envio de nova senha informando email inexistente na base (Painel SMS)
    [tags]                              Regressao         
    Quando clicar em esqueci minha senha
    Então devera preencher o email da conta a ser recuperada        ${invalid_login2}
    Quando clicar em enviar
    Então o sistema deverá exibir a mensagem de que o usario não existe    

Cenario 10: Botão Voltar antes de recuperar senha (Painel SMS)
    [Tags]                              Regressao         
    Quando clicar em esqueci minha senha
    Então devera preencher o email da conta a ser recuperada        ${invalid_login}
    Quando clicar no botão voltar
    Então o sistema devera retornar para a tela principal     

Cenario 11: Login com usuário externo (Painel SMS)
    [Tags]                               Regressao         Daily
    Dado que estou no login unico dos paineis       ${login_externo}                               ${password_externo}
    Quando clicar em "entrar"
    Deverá ver o usuário logado                     ${full_name_dashboard_externo}   

    


***Keywords***


Deverá ver o usuário logado
    [Arguments]                         ${full_name}
    Wait Until Page Contains            ${Full_name}        30

Deverá exibir a mensagem de usuário ou senha invalidos
    Wait Until Page Contains                        Usuário e/ou senha inválidos.          40
    
Então deverá selecionar a empresa
    Wait Until Element is Visible        class=login            10
    Wait Until Element is Visible        id=login_company       10  
    Select From List by Label            idcbemp                ${empresa}
    Wait Until Page Contains             ${empresa}             10

Quando clicar logout na seleção de empresas
    Click Element                       class:fa-sign-out-alt

Então o sistema devera retornar para a tela principal    
    Wait Until Page Contains            Bem vindo ao painel SMS!     10

Então o sistema deverá realizar o login e exibir a dashbord do sistema
    Wait Until Page Contains            ${full_name_dashboard}      30    

Quando realizar o logout na dashbord
    Wait Until Element is Visible   id=nome_usuario     30
    Click Element                   id=nome_usuario     
    Wait Until Element is Visible   id=bt_logout        30
    Click Element                   id=bt_logout        
    

Quando clicar no botão voltar
    Click Element       id=resetback

Quando clicar em esqueci minha senha
    Click Element                   //*[@id="resetpass"]
    Wait Until Page Contains        ${Title1}    

Então devera preencher o email da conta a ser recuperada
    [Arguments]                                                                  ${login_rec}   
    Input Text                      id=email_reset                               ${login_rec}

Então não devera preencher o email da conta a ser recuperada
    Page Should Contains Element    id=email_reset

Então o sistema deverá exibir a mensagem de que o usario não existe
    Wait Until Page Contains             Usuário não existe, favor entrar em contato com o Administrador da sua empresa.        40                 
Quando clicar em enviar    
    Click Element                   xpath=//*[@id="login_recovery"]/div[2]/button
Então Devera realizar a recuperação da senha    
    Wait Until Page Contains        ${Title2}           50
    ${teste}                        Get Value                                      id=email_reset
    Should Be Equal As Strings      ${teste}                                       ${login}
Quando clicar em voltar
    Wait until Page Contains Element        id=resetback      10
    Click Element                           id=resetback

Então deverá ser exibida a mensagem de erro
    Wait Until Page Contains        O login informado não é um e-mail, favor informar um e-mail         40

Reset User Password Null
    Input Text                      xpath=//*[@id="email_reset"]                   ${invalid_login}
    Click Element                   xpath=//*[@id="login_recovery"]/div[2]/button                           
    Wait Until Page Contains        ${title4}

Quando clicar em "confirmar"
    Click Element       xpath=//*[@id="login_company"]/div[2]/button    


    
