*** Settings ***
Resource        ../basesms.robot
Test Setup      Nova sessão     SMS
Test Teardown   Encerra Sessão



*** Variables ***
#Botoes de ação campanha
${BOTAO_PAUSAR}                    id=btPausa
${BOTAO_REINICIAR}                 id=btReiniciar
${BOTAO_CANCELAR}                  id=btCancel
${BOTAO_ESCALONAR}                 id=btEscal

#Login usuario interno cenários
${login}                                        teste@sms.com
${password}                                     Pgmais123
${full_name}                                    Teste Sms
${empresa}                                      QA PgMais
${login_externo}                                qaexterno@sms.com
${password_externo}                             Pgmais123   
${fullname_externo}                             Teste Externo     

# Campanha Regressao
${carteira_regressao}                           rota5000
${carteira_interativo_regressao}                rota5000
${produto_regressao}                            LA  RADAR DLR        
${template_mensagem_regressao}                  templateTesteQA          
${layout_arquivo_regressao}                     Layout QA sem frase NOVO
${arquivo_regressao}                            C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHARADARSMS.txt
${template_mensagem_radar_codigo_regressao}     1234QA
${qtd_lotes_regressao}                          1
${qtd_envios_lote_regressao}                    3
${agendamento_envios_regressao}                 0
${produto_interativo_regressao}                 LA INTERATIVO DLR
${template_regressao_mensagem_interativo}       TESTE DE QA INTERATIVO PRIMEIRA MENSAGEM - Digite S
${data_interativo_regressao}                    31122050
${hora_interativo_regressao}                    1800
${qtd_lotes_regressao}                          1
${qtd_envios_lote_regressao}                    3
${agendamento_envios_regressao}                 0 

# Campanha Radar
${carteira_radar}                               rota5000
${produto_radar}                                LA  RADAR DLR        
${template_mensagem_radar}                      templateTesteQA          
${layout_arquivo_radar}                         Layout QA sem frase NOVO
${arquivo_radar}                                C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHARADARSMS.txt
${qtd_lotes_radar}                              1
${qtd_envios_lote_radar}                        3
${agendamento_envios_radar}                     0
${qtd_envios_lote_menor}                        1
${qtd_envios_lote_maior}                        4
${qtd_lotes_campanhas_teste}                    2

#Campanha Interativo
${carteira_interativo}                          rota5000
${produto_interativo}                           LA INTERATIVO DLR
${template_mensagem_interativo}                 TESTE DE QA INTERATIVO PRIMEIRA MENSAGEM - Digite S
${data_interativo}                              31122050                  
${hora_interativo}                              1800
${layout_arquivo_interativo}                    Layout QA sem frase NOVO
${arquivo_interativo}                           C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHAINTERATIVOSMS.txt
${qtd_lotes_interativo}                         1
${qtd_envios_lote_interativo}                   3
${agendamento_envios_interativo}                0

#Campanha Concatenado
${carteira_concatenado}                         rota5000
${produto_concatenado}                          LA CONCATENADO
${template_mensagem_concatenado}                Ola, [CAMPO3]. Sou a Assistente Virtual da Porto Seguro e essa mensagem se refere a vistoria para contratacao do seguro para o veiculo [CAMPO4], placa [CAMPO5]. A vistoria e realizada atraves do aplicativo Vistoria - Previa Porto Seguro, onde serao solicitadas fotos do veiculo, basta baixar nosso aplicativo e seguir o passo a passo. E Simples, Rapida e Segura!Para celulares com sistema Android clique aqui http;//porto.vc/vpPara celulares com sistema IOS clique aqui http;//porto.vc/vpi
${layout_arquivo_concatenado}                   Layout QA sem frase NOVO
${arquivo_concatenado}                          C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHACONCATENADOSMS.txt
${qtd_lotes_concatenado}                        1
${qtd_envios_lote_concatenado}                  3
${agendamento_envios_concatenado}               0

#Campanha 500 Mil
${carteira_500k}                                rota5000
${produto_500k}                                 LA  RADAR DLR
${template_mensagem_500k}                       templateTesteQA          
${layout_arquivo_500k}                          Layout QA sem frase NOVO
${arquivo_500k}                                 C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHA500K.txt
${tempo_envio_500k}                             660
${qtd_lotes_500k}                               1
${qtd_envio_500k}                               545890
${agendamento_envios_500k}                      0

#Campanha 500k interativo
${carteira_500k_interativo}                     rota5000
${produto_500k_interativo}                      LA INTERATIVO DLR
${template_mensagem_500k_interativo}            TESTE DE QA INTERATIVO PRIMEIRA MENSAGEM - Digite S
${data_interativo_500k}                         31122050    
${hora_interativo_500k}                         1800
${layout_arquivo_500k_interativo}               Layout QA sem frase NOVO
${arquivo_500k_interativo}                      C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHA500K.txt
${tempo_envio_500k_interativo}                  3600 
${qtd_lotes_500k_interativo}                    1
${qtd_envio_500k_interativo}                    545890
${agendamento_envios_500k_interativo}           0

#Campanha 500k Concatenado
${carteira_500k_concatenado}                    rota5000
${produto_500k_concatenado}                     LA CONCATENADO
${template_mensagem_500k_concatenado}           Ola, [CAMPO3]. Sou a Assistente Virtual da Porto Seguro e essa mensagem se refere a vistoria para contratacao do seguro para o veiculo [CAMPO4], placa [CAMPO5]. A vistoria e realizada atraves do aplicativo Vistoria - Previa Porto Seguro, onde serao solicitadas fotos do veiculo, basta baixar nosso aplicativo e seguir o passo a passo. E Simples, Rapida e Segura!Para celulares com sistema Android clique aqui http;//porto.vc/vpPara celulares com sistema IOS clique aqui http;//porto.vc/vpi
${layout_arquivo_500k_concatenado}              Layout QA sem frase NOVO
${arquivo_500k_concatenado}                     C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHA500K.txt
${tempo_envio_500k_concatenado}                 3600 
${qtd_lotes_500k_concatenado}                   1
${qtd_envio_500k_concatenado}                   545890
${agendamento_envios_500k_concatenado}          0

#Campanha 1 Milhão
${carteira_1m}                                  rota5000
${produto_1m}                                   LA  RADAR DLR       
${template_mensagem_1m}                         templateTesteQA          
${layout_arquivo_1m}                            Layout QA sem frase NOVO
${arquivo_1m}                                   C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHA1M.txt
${tempo_envio_1m}                               3600
${qtd_lotes_1m}                                 1
${qtd_envio_1m}                                 545890
${agendamento_envios_1m}                        0

#Campanha 1 Milhão Interativo
${carteira_1m_interativo}                       rota5000
${produto_1m_interativo}                        LA INTERATIVO DLR        
${template_mensagem_1m_interativo}              TESTE DE QA INTERATIVO PRIMEIRA MENSAGEM - Digite S
${data_interativo_1m}                           31122050    
${hora_interativo_1m}                           1800             
${layout_arquivo_1m_interativo}                 Layout QA sem frase NOVO
${arquivo_1m_interativo}                        C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHA1M.txt
${tempo_envio_1m_interativo}                    3600
${qtd_lotes_1m_interativo}                      1
${qtd_envio_1m_interativo}                      545890
${agendamento_envios_1m_interativo}             0

#Campanha 1 Milhão Concatenado
${carteira_1m_concatenado}                      rota5000
${produto_1m_concatenado}                       LA CONCATENADO        
${template_mensagem_1m_concatenado}             Ola, [CAMPO3]. Sou a Assistente Virtual da Porto Seguro e essa mensagem se refere a vistoria para contratacao do seguro para o veiculo [CAMPO4], placa [CAMPO5]. A vistoria e realizada atraves do aplicativo Vistoria - Previa Porto Seguro, onde serao solicitadas fotos do veiculo, basta baixar nosso aplicativo e seguir o passo a passo. E Simples, Rapida e Segura!Para celulares com sistema Android clique aqui http;//porto.vc/vpPara celulares com sistema IOS clique aqui http;//porto.vc/vpi          
${layout_arquivo_1m_concatenado}                Layout QA sem frase NOVO
${arquivo_1m_concatenado}                       C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHA1M.txt
${tempo_envio_1m_concatenado}                   3600
${qtd_lotes_1m_concatenado}                     1
${qtd_envio_1m_concatenado}                     545890
${agendamento_envios_1m_concatenado}            0


#Campanha teste de Blacklist
${carteira_blacklist}                           rota5000
${produto_blacklist}                            LA  RADAR DLR        
${template_mensagem_blacklist}                  templateTesteQA          
${layout_arquivo_blacklist}                     Layout QA sem frase NOVO
${arquivo_blacklist}                            C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHABLACKLIST.txt
${blacklist_counter}                            1
${qtd_lotes_blacklist}                          1
${qtd_envios_lote_blacklist}                    3
${agendamento_envios_blacklist}                 0

#Campanha teste valores repetidos
${carteira_repetido}                           LA - SMS RADAR - QA PgMais
${produto_repetido}                            WTS MENSAGEM        
${template_mensagem_repetido}                  templateTesteQA          
${layout_arquivo_repetido}                     Layout QA sem frase NOVO
${arquivo_repetido}                            C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHAREPETIDO.txt
${contador_de_repetido}                        1
${qtd_lotes_repetido}                          1
${qtd_envios_lote_repetido}                    3
${agendamento_envios_repetido}                 0

#Campanha teste valores invalidos
${carteira_invalido}                           LA - SMS RADAR - QA PgMais
${produto_invalido}                            WTS MENSAGEM        
${template_mensagem_invalido}                  templateTesteQA          
${layout_arquivo_invalido}                     Layout QA sem frase NOVO
${arquivo_invalido}                            C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHAINVALIDO.txt
${contador_de_invalido}                        1
${qtd_lotes_invalido}                          1
${qtd_envios_lote_invalido}                    3
${agendamento_envios_invalido}                 0

#Validação Alertas
${alerta_passo_1_todos_os_campos}               Selecione o Cliente.
${alerta_passo_1_template}                      Selecione o Template.
${alerta_passo_1_interativo}                    Preencha corretamente os campos
${alerta_passo_1_sem_arquivo_e_layout}          Selecione os arquivos.
${alerta_passo_1_sem_layout}                    Escolha um Layout

#Validação agendamento invalido
${agendamento_envios_hora_invalida}            3232 

#Teste de pausa-reinicio e cancelamento
${arquivo_pausa}                               C:/Automacao/AutomacaoSMS/PainelSMS/Arquivos/CAMPANHAPAUSA.txt 
${qtd_envios_lote_pausa}                       919
${agendamento_envios_pausa}                    1800                      

#Botao upload campanha
${botao_upload}       xpath=//input[@id="files"]

#Cancelamento de campanha
${CHECKBOX}                        xpath=//*[@id='mgridlote']//tbody/tr[1]//input[@id='checkidcamp']
${BOTAO_CONFIRMAR_CANCELAMENTO}    xpath=//*[@value='Confirmar Cancelamento']

*** Test Cases ***
#Validação de criação de campanha
Cenario 01: Validação de Criar Campanha RADAR
    [tags]                                   Daily      Regressao
    Dado que estou no login de SMS                                                                              ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_radar}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_radar}                  ${qtd_envios_lote_radar}   ${agendamento_envios_radar}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_radar}

Cenario 02: Validação de Criar Campanha INTERATIVO
    [tags]                                   Daily      Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"                                                                                               
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar" 
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 interativo corretamente                                                        ${carteira_interativo}              ${produto_interativo}               ${template_mensagem_interativo}     ${layout_arquivo_interativo}     ${data_interativo}      ${hora_interativo}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_interativo}
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_interativo}             ${qtd_envios_lote_interativo}       ${agendamento_envios_interativo}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_interativo}

Cenario 03: Validação de Criar Campanha Concatenado
    [tags]                                   Daily     Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_concatenado}             ${produto_concatenado}              ${template_mensagem_concatenado}       ${layout_arquivo_concatenado}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_concatenado}
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_concatenado}            ${qtd_envios_lote_concatenado}      ${agendamento_envios_concatenado}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_concatenado} 


Cenario 04: Validação de Criar Campanha Usando Template por Código
    [tags]                                         Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 corretamente utilizando o layout por código                                    ${carteira_regressao}                   ${produto_regressao}           ${template_mensagem_radar_codigo_regressao}     ${layout_arquivo_regressao}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_regressao}
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_regressao}                  ${qtd_envios_lote_regressao}   ${agendamento_envios_regressao}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_regressao}

Cenario 05: Validação de Pausa de campanha
    [tags]                                                  
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_pausa}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2 realizando agendamento                                                         ${qtd_lotes_radar}                  ${qtd_envios_lote_pausa}   ${agendamento_envios_radar}      ${agendamento_envios_pausa}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_radar}                          
    Quando marcar a campanha e clicar em "Pausar"
    Então o sistema exibirá a campanha com o status "Pausa"

Cenario 06: Validação de Reinicio de campanha
    [tags]                     
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando marcar a campanha e clicar em "Reiniciar"
    Então o sistema exibirá a campanha com o status "A Enviar"

Cenario 07: Validação de Escalonamento de campanha
    [tags]                     
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando marcar a campanha e clicar em "Pausar"
    Então o sistema exibirá a campanha com o status "Pausa"
    Quando marcar a campanha e clicar em "Escalonar"
    Então deverá ser exibida a tela de reescalonamento
    Quando marcar os lotes a serem reescalonados e clicar em OK
    Então deve ser exibido o alert "Deseja reescalonar os Lotes selecionados?"
    Então deverá ser exibida a tela para agendamento dos lotes
    Quando Agendar os lotes e clicar em "Confirmação de Envio"
    Então deve ser exibida a tela de "Escalonar Campanha - Passo 3"
    Quando confirmar escalonamento
    Então o sistema exibirá a campanha com o status "A Enviar"
    

Cenario 08: Validação de Cancelamento de campanha
    [tags]                            
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando marcar a campanha e clicar em "Cancelar"
    Quando selecionar o checkbox e clicar em "Confirmar Cancelamento"
    Então deve ser exibido o alert "Deseja cancelar os Lotes selecionados?"
    Então o sistema exibirá a campanha com o status "Cancelada"

Cenario 09: Validação de Criação de Campanha com mais de um lote
    [tags]                                         Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_radar}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2 configurando mais de um lote
    Então as datas agendadas devem ser exibidas na tela
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal e clicar no botão detalhes                        ${template_mensagem_radar}                          
    Então o sistema acessará a tela Detalhes da campanha
    Deverá então ser exibido o numero de lotes corretamente
    Sleep       20

    
#Validação Performance
Cenario 10: Validação de Performance Campanha 500 Mil Radar
    #Tempo Limite = 15 min
    [tags]                                   Performance
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                             
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_500k}        ${produto_500k}             ${template_mensagem_500k}     ${layout_arquivo_500k}
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_500k}         ${tempo_envio_500k}                       
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_500k}       ${qtd_envio_500k}           ${agendamento_envios_500k}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_500k}

Cenario 11: Validação de Performance Campanha 1 Milhão Radar
    #Tempo Limite = 30 min
    [tags]                                   Performance
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"                          
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1" 
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_1m}        ${produto_1m}             ${template_mensagem_1m}     ${layout_arquivo_1m}
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_1m}         ${tempo_envio_1m}
    Então deve ser exibida a página "Importar Job - Passo 2"            
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_1m}       ${qtd_envio_1m}     ${agendamento_envios_1m}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_1m}

Cenario 12: Validação de Performance Campanha 500 Mil Interativo
    #Tempo Limite = 30 min
    [tags]                                   Performance
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 interativo corretamente                                                        ${carteira_500k_interativo}        ${produto_500k_interativo}             ${template_mensagem_500k_interativo}     ${layout_arquivo_500k_interativo}        ${data_interativo_500k}      ${hora_interativo_500k}
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_500k_interativo}         ${tempo_envio_500k_interativo}                       
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_500k_interativo}       ${qtd_envio_500k_interativo}           ${agendamento_envios_500k_interativo}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_500k_interativo}

Cenario 13: Validação de Performance Campanha 1 Milhão Interativo
    #Tempo Limite = 60 min
    [tags]                                   (Aguardando melhoria)
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 interativo corretamente                                                        ${carteira_1m_interativo}        ${produto_1m_interativo}             ${template_mensagem_1m_interativo}     ${layout_arquivo_1m_interativo}    ${data_interativo_1m}      ${hora_interativo_1m}
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_1m_interativo}         ${tempo_envio_1m_interativo}     
    Então deve ser exibida a página "Importar Job - Passo 2"                  
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_1m_interativo}       ${qtd_envio_1m_interativo}     ${agendamento_envios_1m_interativo}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_1m_interativo}

Cenario 14: Validação de Performance Campanha 500 Mil Concatenado
    #Tempo Limite = 15 min
    [tags]                                   Performance
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_500k_concatenado}        ${produto_500k_concatenado}             ${template_mensagem_500k_concatenado}     ${layout_arquivo_500k_concatenado}
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_500k_concatenado}         ${tempo_envio_500k_concatenado}                       
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_500k_concatenado}       ${qtd_envio_500k_concatenado}           ${agendamento_envios_500k_concatenado}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_500k_concatenado}

Cenario 15: Validação de Performance Campanha 1 Milhão Concatenado
    #Tempo Limite = 30 min
    [tags]                                   Performance
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_1m_concatenado}        ${produto_1m_concatenado}             ${template_mensagem_1m_concatenado}     ${layout_arquivo_1m_concatenado}
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_1m_concatenado}         ${tempo_envio_1m_concatenado}     
    Então deve ser exibida a página "Importar Job - Passo 2"                  
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_1m_concatenado}       ${qtd_envio_1m_concatenado}     ${agendamento_envios_1m_concatenado}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_1m_concatenado}

#Usuarios Externos
Cenario 16: Criar Campanha RADAR Usuario Externo
    [tags]                                   Daily      Regressao
    Dado que estou no login unico dos paineis com o usuario externo                                                         ${login_externo}                    ${password_externo}
    Quando clicar em "entrar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 com usuario externo corretamente                                               ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}                                      
    Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"       ${arquivo_radar}                    100    
    Então deve ser exibida a página "Importar Job - Passo 2"                      
    Quando preencher o formulário do passo 2                                                                                ${qtd_lotes_radar}                  ${qtd_envios_lote_radar}   ${agendamento_envios_radar}
    Então deve ser exibida a página "Importar Job - Passo 3"
    Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal                        ${template_mensagem_radar}
#Validações das telas
Cenario 17: Validação de numeros bloqueados na blacklist importados
    [tags]                                         Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar" 
    Então deve ser exibida a página "Importar Job - Passo 1"                             
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_blacklist}                   ${produto_blacklist}           ${template_mensagem_blacklist}     ${layout_arquivo_blacklist}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_blacklist}
    Então deve ser exibida a página "Importar Job - Passo 2"
    Deverá realizar a validação do numero de registros na Blacklist importados                                              ${blacklist_counter}

Cenario 18: Validação de numeros invalidos importados
    [tags]                                              Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar" 
    Então deve ser exibida a página "Importar Job - Passo 1"                             
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_invalido}                   ${produto_invalido}           ${template_mensagem_invalido}     ${layout_arquivo_invalido}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_invalido}
    Então deve ser exibida a página "Importar Job - Passo 2"
    Deverá realizar a validação do número de registros invalidos importados                                                 ${contador_de_invalido}

Cenario 19: Validação de numeros repetidos importados
    [tags]                                        Regressao                                                                    
    Dado que estou no login unico dos paineis                                                                               ${login}                                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar" 
    Então deve ser exibida a página "Importar Job - Passo 1"                             
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_repetido}                   ${produto_repetido}           ${template_mensagem_repetido}     ${layout_arquivo_repetido}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_repetido}
    Então deve ser exibida a página "Importar Job - Passo 2"
    Deverá realizar a validação do número de registros repetidos importados                                                 ${contador_de_repetido}

Cenario 20: Validação de envio com quantidade menor nos lotes do que o existente no arquivo
    [tags]                                                                                                                  Regressao       
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_radar}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2 e informar menos registros que o importado                                     ${qtd_lotes_radar}                  ${qtd_envios_lote_menor}   ${agendamento_envios_radar}
    Então uma mensagem de mensagens faltantes para configurar é exibida

Cenario 21: Validação de envio com quantidade maior nos lotes do que o existente no arquivo
    [tags]                                      Regressao      
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Sleep       3
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_radar}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2 e informar menos registros que o importado                                     ${qtd_lotes_radar}                  ${qtd_envios_lote_maior}   ${agendamento_envios_radar}
    Então uma mensagem de envios a mais é exibida

Cenario 22: Validação de envio de lote sem informar o horario do agendamento
    [tags]                     Regressao                       
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"                              
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_radar}                   ${produto_radar}           ${template_mensagem_radar}     ${layout_arquivo_radar}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_pausa}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando preencher o formulário do passo 2 realizando agendamento                                                         ${qtd_lotes_radar}                  ${qtd_envios_lote_pausa}   ${agendamento_envios_radar}      ${agendamento_envios_hora_invalida}
    Então uma mensagem de hora invalida é exibida
    
    
Cenario 23: Validação do botão voltar passo 1 campanha
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando clicar no botão "voltar" do passo 1
    Então o Sistema deverá exibir a tela "Dashboard"


Cenario 24: Validação do botão voltar passo 1 (Importar arquivo) campanha
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 com usuario externo corretamente                                               ${carteira_regressao}       ${produto_regressao}        ${template_mensagem_regressao}      ${layout_arquivo_regressao}
    Então a página de de upload de arquivo deverá ser exibida
    Quando clicar no botão voltar na página de upload de arquivo
    Então deve ser exibida a página "Importar Job - Passo 1"

    
Cenario 25: Validação do botão voltar passo 2 campanha
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_regressao}                   ${produto_regressao}           ${template_mensagem_regressao}     ${layout_arquivo_regressao}
    Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"                               ${arquivo_regressao}                      
    Então deve ser exibida a página "Importar Job - Passo 2"
    Quando clicar no botão voltar no passo 2
    Então deve ser exibida a página "Importar Job - Passo 1"
    
Cenario 26: Validação do botão de limpar arquivos no passo 1 no upload
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_regressao}                   ${produto_regressao}           ${template_mensagem_regressao}     ${layout_arquivo_regressao}
    Deverá realizar o upload do arquivo da campanha                                                                         ${arquivo_regressao}
    Quando Clicar em "Limpar Arquivos"
    Então todos os arquivos devem ser removidos
      
    
Cenario 27: Validação do formulário passo 1 sem preencher todos os campos obrigatorios
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 sem informar os campos obrigatórios e clicar OK
    Então um alerta de erro deve ser exibido                                                                                ${alerta_passo_1_todos_os_campos}
    
       
Cenario 28: Validação do formulário passo 1 sem preencher o campo template
    [tags]                                   {Aguardando correçao}
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 sem informar o template e clicar OK
    Então um alerta de erro deve ser exibido                                                                                ${alerta_passo_1_template}

    
Cenario 29: Validação do passo 1 sem preencher data fim e horário do interativo
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando preencher o formulário do passo 1 sem informar a data fim e horario do interativo e clicar OK                    ${carteira_interativo_regressao}              ${produto_interativo_regressao}       ${template_regressao_mensagem_interativo}     ${layout_arquivo_regressao}
    Então uma mensagem de erro deverá ser exibida                                                                           ${alerta_passo_1_interativo}
    
Cenario 30: Validação do passo 1 sem preencher horário fim do interativo
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando preencher o formulário do passo 1 sem informar o horario fim do interativo e clicar OK                           ${carteira_interativo_regressao}              ${produto_interativo_regressao}       ${template_regressao_mensagem_interativo}     ${layout_arquivo_regressao}       ${data_interativo_regressao}
    Então uma mensagem de erro deverá ser exibida                                                                           ${alerta_passo_1_interativo}

Cenario 31: Validação do passo 1 sem preencher data fim do interativo
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando preencher o formulário do passo 1 sem informar a data fim do interativo e clicar OK                              ${carteira_interativo_regressao}              ${produto_interativo_regressao}       ${template_regressao_mensagem_interativo}     ${layout_arquivo_regressao}       ${hora_interativo_regressao}
    Então uma mensagem de erro deverá ser exibida                                                                           ${alerta_passo_1_interativo}

Cenario 32: Validação do passo 1 importar arquivo sem preencher os campos obrigatórios
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando inserir os dados no formulário do passo 1 corretamente                                                            ${carteira_regressao}                   ${produto_regressao}           ${template_mensagem_regressao}
    Então a página de importação de arquivo deverá ser exibida
    Quando Clicar em "Passo 2 - Periódos de Envio"
    Então um alerta de erro deve ser exibido                                                                                 ${alerta_passo_1_sem_arquivo_e_layout}                                   


Cenario 33: Validação do passo 1 sem fazer upload de arquivo
    [tags]                                   Regressao

    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando Preencher o formulário do passo 1 corretamente                                                                   ${carteira_regressao}                   ${produto_regressao}           ${template_mensagem_regressao}       ${layout_arquivo_regressao}       
    Então a página de importação de arquivo deverá ser exibida
    Quando Clicar em "Passo 2 - Periódos de Envio"
    Então um alerta de erro deve ser exibido                                                                                ${alerta_passo_1_sem_arquivo_e_layout}                                                                     
    
Cenario 34: Validação do formulario passo 1 importar arquivo sem preencher o layout
    [tags]                                   Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"
    Quando inserir os dados no formulário do passo 1 corretamente                                                           ${carteira_regressao}                   ${produto_regressao}           ${template_mensagem_regressao}
    Realizar upload de arquivo sem selecionar o layout                                                                      ${arquivo_regressao}
    Então um alerta de erro deve ser exibido                                                                                ${alerta_passo_1_sem_layout}

Cenario 35: Validação de acesso a criação de campanhas pelo Dashboard usando o botão "Nova Campanha"
    [tags]                                  Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Nova Campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

Cenario 36: Validação de acesso a criação de campanhas pelo menu "Jobs" -> "Gerenciar"
    [tags]                                  Regressao
    Dado que estou no login unico dos paineis                                                                               ${login}                ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}            ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Sleep       1
    Quando clicar em "Gerenciar"                                                                                            Gerenciar
    Sleep       1                                                                                                           
    Então deve ser exibida a tela de "Gerenciar" campanhas
    Quando Clicar em "Nova Campanha" na tela de Gerenciar
    Então deve ser exibida a página "Importar Job - Passo 1"

    
    



*** Keywords ***
#Acessa a pagina de criação de campanha e valida se os componentes existem
Quando clicar em "Importar"                            
    Wait Until Page Contains Element               id=aba3      10      
    Mouse Over                                     id=aba3
    Wait Until Page Contains Element               xpath=//html/body/div[5]/ul/li[2]/a      10
    Click Element                                  xpath=//html/body/div[5]/ul/li[2]/a 

Quando clicar em "Nova Campanha"
    Wait Until Page Contains Element               id=btInicio
    Click Element                                  id=btInicio

Quando clicar em "Gerenciar"
    [Arguments]                                    ${opcao} 
    Wait Until Page Contains Element               id=aba3      10      
    Mouse Over                                     id=aba3
    Wait Until Page Contains Element               xpath=//div[@class='anylinkmenu']//a[contains(text(),'${opcao}')]       10
    Click Element                                  xpath=//div[@class='anylinkmenu']//a[contains(text(),'${opcao}')]

Então deve ser exibida a tela de "Gerenciar" campanhas
    Wait Until Page Contains                       Gerenciar


Quando Clicar em "Nova Campanha" na tela de Gerenciar
    Wait Until Page Contains Element                xpath=/html/body/div[4]/div/div[2]/div/div[3]/div[2]/div/div/button/span      10
    Click Element                                   xpath=/html/body/div[4]/div/div[2]/div/div[3]/div[2]/div/div/button/span




Então deve ser exibida a página "Importar Job - Passo 1"     
    Wait Until Page Contains                       Importar Job - Passo 1
    Wait Until Page Contains Element               id=select2-idcliente-container   1      
    Wait Until Page Contains Element               id=select2-produto-container     1
    Wait Until Page Contains Element               id=opcao_template                1
    Wait Until Page Contains Element               id=select2-infmsg-container      1
    Wait Until Page Contains Element               id=btOk                          1
    Wait Until Page Contains Element               xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/input[3]  #Botao Voltar
    Capture Page Screenshot

Quando Preencher o formulário do passo 1 corretamente
    #Seleciona a carteira, produto e template e valida se os mesmos sao selecionados corretamente
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}
    Wait Until Page Contains Element               id=idcliente                    2     
    Wait Until Page Contains Element               id=produto                      2 
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2
    Select From List By Label                      id=infmsg                       ${template}
    Sleep                                          2
    Capture Page Screenshot
    Click Element                                  id=btOk
    ${cliente_selecionado}                         Get Selected List Label         id=idcliente
    Should Be Equal as Strings                     ${cliente_selecionado}          ${cliente}
    ${produto_envio_selecionado}                   Get Selected List Label         id=tipo
    Should Be Equal as Strings                     ${produto_envio_selecionado}    ${produto}
    Select From List By Label                      id=idlayout                     ${layout_arquivo}
    ${layout_selecionado}                          Get Selected List Label         id=idlayout

Quando Preencher o formulário do passo 1 corretamente utilizando o layout por código
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}
    Wait Until Page Contains Element               id=idcliente                    2     
    Wait Until Page Contains Element               id=produto                      2 
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2
    Select Radio Button                            opcao_template                  C
    Sleep                                          2
    Select From List By Label                      id=infmsg                       ${template}
    Sleep                                          2
    Capture Page Screenshot
    Click Element                                  id=btOk
    ${cliente_selecionado}                         Get Selected List Label         id=idcliente
    Should Be Equal as Strings                     ${cliente_selecionado}          ${cliente}
    ${produto_envio_selecionado}                   Get Selected List Label         id=tipo
    Should Be Equal as Strings                     ${produto_envio_selecionado}    ${produto}
    Select From List By Label                      id=idlayout                     ${layout_arquivo}
    ${layout_selecionado}                          Get Selected List Label         id=idlayout

Quando inserir os dados no formulário do passo 1 corretamente
    #Seleciona a carteira, produto e template e valida se os mesmos sao selecionados corretamente
    [Arguments]                                    ${cliente}     ${produto}      ${template}
    Wait Until Page Contains Element               id=idcliente                    2     
    Wait Until Page Contains Element               id=produto                      2 
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2
    Select From List By Label                      id=infmsg                       ${template}
    Sleep                                          2
    Capture Page Screenshot
    Click Element                                  id=btOk
    ${cliente_selecionado}                         Get Selected List Label         id=idcliente
    Should Be Equal as Strings                     ${cliente_selecionado}          ${cliente}
    ${produto_envio_selecionado}                   Get Selected List Label         id=tipo
    Should Be Equal as Strings                     ${produto_envio_selecionado}    ${produto}
    ${layout_selecionado}                          Get Selected List Label         id=idlayout

Deverá realizar o upload do arquivo da campanha e clicar em "Passo 2 - períodos de Envio"
    #Seleciona o arquivo para envio
    [Arguments]                                    ${arquivo_campanha}  
    Choose File                                    ${botao_upload}           ${arquivo_campanha}
    Sleep                           5
    Wait Until Page Contains Element               class:k-i-check                 1200 
    Capture Page Screenshot
    Click Element                                  id=clickSubmit

Então deve ser exibida a página "Importar Job - Passo 2"    
    Wait Until Page Contains                       Importar Job - Passo 2          1200

Deverá realizar o upload do arquivo da campanha de teste de performance e clicar em "Passo 2 - períodos de Envio"
    #Seleciona o arquivo para envio
    [Arguments]                                    ${arquivo_campanha}             ${timeout_upload}  
    Choose File                                    ${botao_upload}                 ${arquivo_campanha}                                         
    Wait Until Page Contains Element               class:k-i-check                 ${timeout_upload}
    Capture Page Screenshot
    Click Element                                  id=clickSubmit
         

Quando Preencher o formulário do passo 1 interativo corretamente
    #Passo 1 para criação de interativo (seleciona data fim e horario)
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}   ${data_interativo}      ${horario_interativo}
    Sleep                                          2
    Wait Until Page Contains Element               id=idcliente     2
    Sleep                                          2
    Wait Until Page Contains Element               id=produto       2 
    Sleep                                          2   
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2
    Wait Until Page Contains Element               id=dtpick    
    Input Text                                     id=dtpick                       ${data_interativo}                  
    Wait Until Page Contains Element               id=hora          2
    Input Text                                     id=hora                         ${horario_interativo}
    Click Element                                  xpath=/html/body/div[13]/div[5]/button[2]
    Select From List By Label                      id=infmsg                       ${template}
    Click Element                                  id=btOk
    ${cliente_selecionado}                         Get Selected List Label         id=idcliente
    Should Be Equal as Strings                     ${cliente_selecionado}          ${cliente}
    ${produto_envio_selecionado}                   Get Selected List Label         id=tipo
    Should Be Equal as Strings                     ${produto_envio_selecionado}    ${produto}
    Select From List By Label                      id=idlayout                     ${layout_arquivo}
    ${layout_selecionado}                          Get Selected List Label         id=idlayout


Quando preencher o formulário do passo 2
    #Seleciona o numero de lotes/quantidade e data agendamento
    [Arguments]                                    ${lotes}     ${qtd_por_lote}     ${data_agendamento}
    Wait Until Page Contains                       Importar Job - Passo 2       120
    Wait Until Page Contains Element               id=lotes         2
    Wait Until Page Contains Element               id=qtlot_1       2
    Wait Until Page Contains Element               id=data          2
    Wait Until Page Contains Element               xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[3]       2
    Select From List By Value                      id=lotes                         ${lotes}
    Input Text                                     id=qtlot_1                       ${qtd_por_lote}
    Select From List By Value                      id=data                          ${data_agendamento}
    Capture Page Screenshot
    Click Element                                  xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[2]
Quando preencher o formulário do passo 2 configurando mais de um lote        
    Wait Until Page Contains                       Importar Job - Passo 2       120
    Wait Until Page Contains Element               id=lotes         2
    Wait Until Page Contains Element               id=qtlot_1       2
    Wait Until Page Contains Element               id=data          2
    Wait Until Page Contains Element               xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[3]       2
    Select From List By Value                      id=lotes                             2
    ${data_agendamento}=                           Acrescentar dias à data atual        1 days                        
    Set Global Variable                            ${data_agendamento}
    Select From List By Value                      xpath=//*[@id='mgridlote']//tr/td/strong[contains(text(),'2')]/../..//*[@id='data']      ${data_agendamento}      
    Input Text                                     id=hragend_2         1800
    Click Element                                  xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[2]

Então as datas agendadas devem ser exibidas na tela
    Wait Until Page Contains Element                                                           xpath=//h5[contains(text(),'Importar Job - Passo 3')]
    Page Should Contain Element                                                                xpath=//strong[contains(text(),'Lote 1')]
    Page Should Contain Element                                                                xpath=//strong[contains(text(),'Envio Imediato!')]
    Page Should Contain Element                                                                xpath=//strong[contains(text(),'Lote 2')]
    Page Should Contain Element                                                                xpath=//strong[contains(text(),'Agendado para: ')]
    Page Should Contain                                                                        ${data_agendamento} - 18:00



Quando preencher o formulário do passo 2 e informar menos registros que o importado
    [Arguments]                                    ${lotes}     ${qtd_por_lote_menor2}     ${data_agendamento}
    Wait Until Page Contains                       Importar Job - Passo 2       120
    Wait Until Page Contains Element               id=lotes         2
    Wait Until Page Contains Element               id=qtlot_1       2
    Wait Until Page Contains Element               id=data          2
    Wait Until Page Contains Element               xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[3]       2
    Select From List By Value                      id=lotes                         ${lotes}
    Input Text                                     id=qtlot_1                       ${qtd_por_lote_menor2}
    #${data_agendamento}=                           Acrescentar dias à data atual        1 days
    Select From List By Value                      id=data                          ${data_agendamento}
    Capture Page Screenshot
    Click Element                                  xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[2]

Quando preencher o formulário do passo 2 realizando agendamento
    #Seleciona o numero de lotes/quantidade e data agendamento
    [Arguments]                                    ${lotes}     ${qtd_por_lote}     ${data_agendamento}     ${horas_agendamento}
    Wait Until Page Contains                       Importar Job - Passo 2       120
    Wait Until Page Contains Element               id=lotes         2
    Wait Until Page Contains Element               id=qtlot_1       2
    Wait Until Page Contains Element               id=data          2
    Wait Until Page Contains Element               xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[3]       2
    Select From List By Value                      id=lotes                         ${lotes}
    Input Text                                     id=qtlot_1                       ${qtd_por_lote}
    ${data_agendamento}=                           Acrescentar dias à data atual    1 days
    Select From List By Value                      id=data                          ${data_agendamento}
    Input Text                                     id=hragend_1                     ${horas_agendamento}                      
    Capture Page Screenshot
    Click Element                                  xpath=//html/body/div[4]/table/tbody/tr/td[3]/form/div[4]/input[2]

    
Então deve ser exibida a página "Importar Job - Passo 3"    
    Wait Until Page Contains                       Importar Job - Passo 3       120

Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal
    #Valida se exemplo de mensagem é exibida na tela e confirma a criação da campanha
    [Arguments]                                    ${template}
    Wait Until Page Contains                       Importar Job - Passo 3       120
    Wait Until Page Contains Element               id=SMSEx                     120
    Page Should Contain                            ${template}
    ${idcampanha}                                  Get Value                        xpath=/html/body/div[4]/table/tbody/tr/td[3]/table/tbody/tr[2]/td/form/input[7]                                      
    Set Global Variable                            ${idcampanha}
    Log To Console                                 ${idcampanha}
    Capture Page Screenshot
    Click Element                                  class:valida    
    Wait Until Page Contains                       Campanhas em andamento       2
    Wait Until Page Contains                       ${idcampanha}                30
    #Wait Until Page Contains Element               xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]/../../td[4]/span[contains(text(), 'A Enviar')]    1200  
    Capture Page Screenshot    

Quando clicar em "Confirmar Envio" a campanha deve ser criada e retornar para a página principal e clicar no botão detalhes
    #Valida se exemplo de mensagem é exibida na tela e confirma a criação da campanha
    [Arguments]                                    ${template}
    Wait Until Page Contains                       Importar Job - Passo 3       120
    Wait Until Page Contains Element               id=SMSEx                     120
    Page Should Contain                            ${template}
    ${idcampanha}                                  Get Value                        xpath=/html/body/div[4]/table/tbody/tr/td[3]/table/tbody/tr[2]/td/form/input[7]                                      
    Set Global Variable                            ${idcampanha}
    Log To Console                                 ${idcampanha}
    Capture Page Screenshot
    Click Element                                  class:valida    
    Wait Until Page Contains                       Campanhas em andamento       2
    Wait Until Page Contains                       ${idcampanha}                2
    #Wait Until Page Contains Element               xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]/../../td[4]/span[contains(text(), 'A Enviar')]    1200  
    Capture Page Screenshot
    Click Element                                  id=view_${idcampanha}
    Wait Until Page Contains                       ${idcampanha}                120
Então o sistema acessará a tela Detalhes da campanha
    Wait Until Page Contains                Jobs
    Wait Until Page Contains                Detalhes

Deverá então ser exibido o numero de lotes corretamente
    Page Should Contain     Lotes de Envio: 2                              

Quando marcar a campanha e clicar em "${acao}"
    Select Checkbox     xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]/../../td[1]/input[@id='checkidcamp']
    Run Keyword If                                                                             '${acao}'=='Pausar'                                                                                            Run Keywords                                             Click Element                                                                                                         ${BOTAO_PAUSAR}                                                                                                          AND                          Alert Should Be Present    text=Deseja pausar a Campanha selecionada?
    ...                                                                                        ELSE IF                                                                                                        '${acao}'=='Reiniciar'                                   Run Keywords                                                                                                          Click Element                                                                                                            ${BOTAO_REINICIAR}           AND                        Alert Should Be Present                       text=Deseja reiniciar a Campanha selecionada?
    ...                                                                                        ELSE IF                                                                                                        '${acao}'=='Cancelar'                                    Run Keywords                                                                                                          Click Element                                                                                                            ${BOTAO_CANCELAR}            AND                        Alert Should Be Present                       text=Deseja cancelar a Campanha selecionada?
    ...                                                                                        ELSE IF                                                                                                        '${acao}'=='Escalonar'                                   Run Keywords                                                                                                          Click Element                                                                                                            ${BOTAO_ESCALONAR}           AND                        Alert Should Be Present                       text=Deseja reescalonar a Campanha selecionada?        
    Capture Page Screenshot


Então o sistema exibirá a campanha com o status "${statuscampanha}"
    Page Should Contain Element                                                                xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]
    Run Keyword If                                                                             '${statuscampanha}'=='Pausa'                                                                                           Wait Until Page Contains Element                         xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]/../../td[4]/span[contains(text(), 'Pausa')]    120s
    ...                                                                                        ELSE IF                                                                                                        '${statuscampanha}'=='A Enviar'                                  Wait Until Page Contains Element                                                                                      xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]/../../td[4]/span[contains(text(), 'A Enviar')]    120s
    ...                                                                                        ELSE IF                                                                                                        '${statuscampanha}'=='Cancelada'                                  Wait Until Page Contains Element                                                                                      xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]/../../td[4]/span[contains(text(), 'Cancelada')]    120s

Então deverá ser exibida a tela de reescalonamento
    Page Should Contain         Escalonar Campanha - Passo 2

Quando marcar os lotes a serem reescalonados e clicar em OK
    Select Checkbox             id=checkidcamp
    Click Element               id=btOK
Então deverá ser exibida a tela para agendamento dos lotes
    Page Should Contain         Escolha a quantidade de lotes a enviar e faça seu agendamento.
    Page Should Contain         Os campos marcados com (*) são de preenchimento obrigatório.
Quando Agendar os lotes e clicar em "Confirmação de Envio"
    ${data_agendamento}=                           Acrescentar dias à data atual    1 days
    Select From List By Value                      id=data                          ${data_agendamento}
    Input Text                                     id=hragend_1                     1800
    Click Element                                  xpath=//*[@id="actbuttons"]/input[1]

Então deve ser exibida a tela de "Escalonar Campanha - Passo 3"
    Wait Until Page Contains         Escalonar Campanha - Passo 3       20
Quando confirmar escalonamento
    Click Element       id=botao_submit            





Quando selecionar o checkbox e clicar em "Confirmar Cancelamento"
    Click Element                                                                              ${CHECKBOX}
    Click Element                                                                              ${BOTAO_CONFIRMAR_CANCELAMENTO}


Então a campanha não deve ser exibida
    Page Should Not Contain Element                                                            xpath=//table[@id='mgrid']//td/span[contains(text(),'${idcampanha}')]

Então deve ser exibido o alert "${TEXTO_ALERT}"
    Alert Should Be Present                                                                    ${TEXTO_ALERT}





Quando Preencher o formulário do passo 1 com usuario externo corretamente
    #Seleciona a carteira, produto e template e valida se os mesmos sao selecionados corretamente
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}
    Wait Until Page Contains Element               id=idcliente                    2     
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=infmsg                       ${template}
    Sleep                                          2
    Click Element                                  id=btOk
    ${cliente_selecionado}                         Get Selected List Label         id=idcliente
    Should Be Equal as Strings                     ${cliente_selecionado}          ${cliente}
    Select From List By Label                      id=idlayout                     ${layout_arquivo}
    ${layout_selecionado}                          Get Selected List Label         id=idlayout                              
    

Deverá realizar a validação do numero de registros na Blacklist importados
    [Arguments]                                                ${blacklist_counter3}             
    ${blacklist_counter2}                        Get Text      xpath=/html/body/div[4]/table/tbody/tr/td[3]/table[2]/tbody/tr/td[5]
    Log To Console                               ${blacklist_counter2}
    Should Be Equal as Strings                   ${blacklist_counter2}      ${blacklist_counter3}

Deverá realizar a validação do número de registros repetidos importados
    [Arguments]                                                                 ${contador_de_repetidos3}          
    ${contador_de_repetidos2}                        Get Text      xpath=/html/body/div[4]/table/tbody/tr/td[3]/table[2]/tbody/tr/td[6]
    Log To Console                               ${contador_de_repetidos2}
    Should Be Equal as Strings                   ${contador_de_repetidos2}      ${contador_de_repetidos3}

Deverá realizar a validação do número de registros invalidos importados
    [Arguments]                                                                 ${contador_de_invalidos3}          
    ${contador_de_invalidos2}                        Get Text      xpath=/html/body/div[4]/table/tbody/tr/td[3]/table[2]/tbody/tr/td[4]
    Log To Console                               ${contador_de_invalidos2}
    Should Be Equal as Strings                   ${contador_de_invalidos2}      ${contador_de_invalidos3}            


Quando clicar no botão "voltar" do passo 1
    Click Element       xpath=//*[@id="frmImp"]/input[3]

Então a página de de upload de arquivo deverá ser exibida
    Wait Until Page Contains                    Importar Job - Passo 1      120

Quando clicar no botão voltar na página de upload de arquivo
    Click Element       xpath=//*[@id="actbuttons"]/input[3]

Quando clicar no botão voltar no passo 2
    Click Element       xpath=//*[@id="actbuttons"]/input[3]

Deverá realizar o upload do arquivo da campanha
    #Seleciona o arquivo para envio
    [Arguments]                                    ${arquivo_campanha}  
    Choose File                                    ${botao_upload}           ${arquivo_campanha}
    Wait Until Page Contains Element               class:k-i-check                 1200

Quando Clicar em "Limpar Arquivos"
    Click Element       id=clearAll

Então todos os arquivos devem ser removidos
    Wait Until Page Does Not Contain Element           class:k-i-check                 1200

Quando Preencher o formulário do passo 1 sem informar os campos obrigatórios e clicar OK
    Wait Until Page Contains Element               id=idcliente                    2     
    Wait Until Page Contains Element               id=produto                      2 
    Click Element                                  id=btOk

Quando Preencher o formulário do passo 1 sem informar o template e clicar OK
    [Arguments]                                    ${cliente}     ${produto}      ${template}
    Wait Until Page Contains Element               id=idcliente                    2     
    Wait Until Page Contains Element               id=produto                      2 
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2
    Select From List By Label                      id=infmsg                       ${template}
    Sleep                                          2
    Capture Page Screenshot
    Click Element                                  id=btOk 

Quando preencher o formulário do passo 1 sem informar a data fim e horario do interativo e clicar OK
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}
    Wait Until Page Contains Element               id=idcliente     2
    Wait Until Page Contains Element               id=produto       2 
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          3
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          3             
    Select From List By Label                      id=infmsg                       ${template}
    Click Element                                  id=btOk

Quando preencher o formulário do passo 1 sem informar o horario fim do interativo e clicar OK
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}   ${data_interativo}
    Sleep                                          2
    Wait Until Page Contains Element               id=idcliente     2
    Sleep                                          2
    Wait Until Page Contains Element               id=produto       2 
    Sleep                                          2   
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2
    Wait Until Page Contains Element               id=dtpick    
    Input Text                                     id=dtpick                       ${data_interativo}                  
    Click Element                                  xpath=/html/body/div[13]/div[5]/button[2]
    Select From List By Label                      id=infmsg                       ${template}
    Click Element                                  id=btOk

Quando preencher o formulário do passo 1 sem informar a data fim do interativo e clicar OK
    [Arguments]                                    ${cliente}     ${produto}      ${template}   ${layout_arquivo}   ${horario_interativo}
    Sleep                                          2
    Wait Until Page Contains Element               id=idcliente     2
    Sleep                                          2
    Wait Until Page Contains Element               id=produto       2 
    Sleep                                          2   
    Select From List By Label                      id=idcliente                    ${cliente}
    Sleep                                          2
    Select From List By Label                      id=produto                      ${produto}
    Sleep                                          2                 
    Wait Until Page Contains Element               id=hora          2
    Input Text                                     id=hora                         ${horario_interativo}
    Select From List By Label                      id=infmsg                       ${template}
    Click Element                                  id=btOk         

Então um alerta de erro deve ser exibido
    [Arguments]                             ${texto_alerta}
    Alert Should Be Present                 ${texto_alerta}

Então uma mensagem de erro deverá ser exibida

    [Arguments]                             ${texto_alerta}
    Wait Until Page Contains                ${texto_alerta}

Então a página de importação de arquivo deverá ser exibida
    Page Should Contain                     Importação de arquivos

Então uma mensagem de mensagens faltantes para configurar é exibida
    Page Should Contain                     Voce ainda possui 2 SMSs para distribuir nos lotes!

Então uma mensagem de envios a mais é exibida
    Page Should Contain                     O total informado não está correto. você possui 1 SMSs a mais que o total do arquivo.

Então uma mensagem de hora invalida é exibida
    Wait Until Page Contains                     Hora Inválida!      10            

Quando Clicar em "Passo 2 - Periódos de Envio"
    CLick Element                           id=clickSubmit

Realizar upload de arquivo sem selecionar o layout
    Sleep                                          5                                        
    [Arguments]                                    ${arquivo_campanha}
    Wait Until Page Contains Element               ${botao_upload}                 5    
    Choose File                                    ${botao_upload}           ${arquivo_campanha}

    