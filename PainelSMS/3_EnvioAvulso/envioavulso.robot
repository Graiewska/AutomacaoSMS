*** Settings ***
Resource        ../basesms.robot
Test Setup      Nova sessão     PAINEL
Test Teardown   Encerra Sessão

*** Variables ***
${login}                            teste@sms.com
${password}                         Pgmais123
${full_name_dashboard}              Teste Sms
${telefones_avulso}                 41988975759,41999882914
*** Test Cases ***
Cenario 01: Validando acesso a tela de envio rapido
    [tags]              
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio Rápido
    Então Deverá Ser Carregada A Tela De Envio Rápido

Cenario 02: Validando envio rapido simples
    [tags]                         
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio Rápido
    Então Deverá Ser Carregada A Tela De Envio Rápido    
    Quando selecionar a carteira e inserir os dados e clicar enviar                                                         LA - SMS RADAR - QA PgMais          ${telefones_avulso}
    Então deve ser exibido o alert "Deseja Enviar?"
    Então o sistema deverá exibir a "mensagens enviadas com sucesso."
Cenario 03: Validando envio rapido simples com numero no blacklist
    [tags] - necessita correção    
Cenario 04: Validando envio rapido simples sem informar telefones
    [tags]              [Corrigir]
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio Rápido
    Então Deverá Ser Carregada A Tela De Envio Rápido    
    Quando selecionar a carteira e não inserir os telefones e clicar enviar                                                 LA - SMS RADAR - QA PgMais
    Então será exibido o alert
Cenario 05: Validando envio rapido simples sem informar carteira
    [tags]              
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio Rápido
    Então Deverá Ser Carregada A Tela De Envio Rápido    
    Quando NÃO selecionar a carteira e inserir os dados clicar enviar                                                       ${telefones_avulso}
    Então deve ser exibido o alert "Selecione um Cliente"
Cenario 06: Validando envio rapido simples sem informar mensagem
    [tags]              
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio Rápido
    Então Deverá Ser Carregada A Tela De Envio Rápido    
    Quando selecionar a carteira e não inserir a mensagem e clicar enviar                                                   LA - SMS RADAR - QA PgMais          ${telefones_avulso}
    Sleep           3
    Então deve ser exibido o alert "Digite o conteúdo da mensagem."
Cenario 07: Validando envio rapido simples com todos os campos em branco
    [tags]              [Corrigir]
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio Rápido
    Então Deverá Ser Carregada A Tela De Envio Rápido    
    Quando não preencher nenhum dos campos e clicar enviar
    Então será exibido o alert

Cenario 08: Validando acesso a tela de envio rapido concatenado
    [tags]              
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio rápido concatenado
    Então deverá ser carregada a tela de envio rápido concatenado
Cenario 09: Validando envio rapido concatenado
    [tags]                     
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio rápido concatenado
    Então deverá ser carregada a tela de envio rápido concatenado    
    Quando selecionar a carteira , inserir os dados , mensagem concatenada e clicar enviar                                  LA DLR - SMS CONCATENADO - QA PgMais        ${telefones_avulso}
    Então o sistema deverá exibir a "mensagens enviadas com sucesso."
Cenario 10: Validando envio rapido concatenado com numero no blacklist 
Cenario 11: Validando envio rapido concatenado sem informar telefones
    [tags]              [Correção]
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio rápido concatenado
    Então deverá ser carregada a tela de envio rápido concatenado    
    Quando selecionar a carteira e não inserir os telefones e clicar enviar                                                 LA - SMS RADAR - QA PgMais
    
Cenario 12: Validando envio rapido concatenado sem informar carteira
    [tags]              
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio rápido concatenado
    Então deverá ser carregada a tela de envio rápido concatenado    
    Quando NÃO selecionar a carteira e inserir os dados clicar enviar                                                       ${telefones_avulso}                                                       
    Então deve ser exibido o alert "Selecione um Cliente"
Cenario 13: Validando envio rapido concatenado sem informar mensagem
    [tags]              
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio rápido concatenado
    Então deverá ser carregada a tela de envio rápido concatenado    
    Quando selecionar a carteira e não inserir a mensagem e clicar enviar                                                   LA - SMS RADAR - QA PgMais          ${telefones_avulso}
    Então deve ser exibido o alert "Digite o conteúdo da mensagem."
Cenario 14: Validando envio rapido concatenado com todos os campos em branco
    [tags]              [Correção]
    Dado que estou no login unico dos paineis                                                                               ${login}                            ${password}
    Quando clicar em "entrar"
    Então deve ser exibida a tela de "seleção de empresas"                                                                  ${full_name}                        ${empresa}
    Quando clicar em "confirmar"
    Então o Sistema deverá exibir a tela "Dashboard"
    Quando clicar em "Envio Rápido"                                                                                         Envio rápido concatenado
    Então deverá ser carregada a tela de envio rápido concatenado    
    Quando não preencher nenhum dos campos e clicar enviar


    

*** Keywords ***
Quando clicar em "Envio Rápido"
    [Arguments]                                    ${opcao} 
    Wait Until Page Contains Element               id=aba3      10      
    Mouse Over                                     id=aba3
    Wait Until Page Contains Element               xpath=//div[@class='anylinkmenu']//a[contains(text(),'${opcao}')]       10
    Click Element                                  xpath=//div[@class='anylinkmenu']//a[contains(text(),'${opcao}')]

Então deverá ser carregada a tela de envio rápido
    Wait Until Page Contains                       Envio Avulso             10
    Wait Until Page Contains Element               id=carteiraSelect        10    
    Wait Until Page Contains Element               id=telefones             10
    Wait Until Page Contains Element               id=msg                   10
    Wait Until Page Contains Element               id=btOK
Então deverá ser carregada a tela de envio rápido concatenado 
    Wait Until Page Contains                       Campanhas                    10
    Wait Until Page Contains                       Envio Rápido Concatenado     10
    Wait Until Page Contains Element               id=idemp                     10    
    Wait Until Page Contains Element               id=telefones                 10
    Wait Until Page Contains Element               id=msg                       10
    Wait Until Page Contains Element               id=btOK                      10
Quando selecionar a carteira e inserir os dados e clicar enviar
    [Arguments]                                    ${carteira}                     ${telefones}
    Select From List By Label                      id=idemp                        ${carteira}
    Input Text                                     id=telefones                    ${telefones}
    Input Text                                     id=msg                          TesteQAAutomacao Avulso Simples
    Click Element                                  id=btOK
    
Quando selecionar a carteira , inserir os dados , mensagem concatenada e clicar enviar
    [Arguments]                                    ${carteira}                     ${telefones}
    Select From List By Label                      id=idemp                        ${carteira}
    Input Text                                     id=telefones                    ${telefones}
    Input Text                                     id=msg                          TesteQAAutomacaoCONCATENADATesteQAAutomacaoCONCATENADATesteQAAutomacaoCONCATENADATesteQAAutomacaoCONCATENADATesteQAAutomacaoCONCATENADATesteQAAutomacaoCONCATENADA TESTE ENVIO AVULSO CONCA
    Click Element                                  id=btOK    
Então deve ser exibido o alert "${texto_do_alerta}"
    Alert Should Be Present                        ${texto_do_alerta}
Então será exibido o alert
    Alert Should Be Present                        Erros Encontrados:  Número não é um celular:  Efetue a correção para enviar a mensagem!
Então o sistema deverá exibir a "${mensagem}"
    Wait Until Page Contains                       ${mensagem}     20    
    
Quando selecionar a carteira e não inserir os telefones e clicar enviar
    [Arguments]                                    ${carteira}                     
    Select From List By Label                      id=idemp                        ${carteira}
    Input Text                                     id=msg                          TesteQAAutomacao
    Click Element                                  id=btOK

Quando NÃO selecionar a carteira e inserir os dados clicar enviar
    [Arguments]                                    ${telefones}
    Input Text                                     id=telefones                    ${telefones}
    Input Text                                     id=msg                          TesteQAAutomacao
    Click Element                                  id=btOK    
Quando selecionar a carteira e não inserir a mensagem e clicar enviar
    [Arguments]                                    ${carteira}                     ${telefones}
    Select From List By Label                      id=idemp                        ${carteira}
    Input Text                                     id=telefones                    ${telefones}
    Click Element                                  id=btOK      

Quando não preencher nenhum dos campos e clicar enviar
    Click Element                                  id=btOK

Então o sistema NÃO deverá exibir a "${mensagem}"
    Wait Until Page Contains                       ${mensagem}     20    

    