*** Settings ***
Resource        ../basesms.robot
Test Setup      Nova sessão
Test Teardown   Encerra Sessão

*** Test Cases ***
Validar Componentes Dashboard
    [Tags]                               Teste_Positivo
    Login With
    Select Product and Company
    Sleep                                3
    Validate Dashboard Tooltips


Validar botão ajuda (News)
    [Tags]                               Teste_Positivo1
    Login With
    Select Product and Company
    Sleep                                3
    Help Button Access
    Access News
    News Validate

Validar botão ajuda (Tutorial)
    [Tags]                               Teste_Positivo2
    Login With
    Select Product and Company
    Sleep                                3
    Help Button Access
    Access Tutorial
    Tutorial Validate

    
    

    

*** Keywords ***
Validate Dashboard Tooltips
    
    
    Wait Until Page Contains Element	xpath=//html/body/div[4]/a
    Wait Until Page Contains            Campanhas em andamento
    Wait Until Page Contains            Importados
    Wait Until Page Contains            Carregados
    Wait Until Page Contains            Enviados    
    Wait Until Page Contains Element    id=btInicio
    Wait Until Page Contains Element    id=btPausa
    Wait Until Page Contains Element    id=btReiniciar
    Wait Until Page Contains Element    id=btEscal
    Wait Until Page Contains Element    id=btCancel
    Wait Until Page Contains Element    id=sh_cliente
    Wait Until Page Contains Element    id=filtro
    Wait Until Page Contains Element    id=gridcampanha
      



Help Button Access
	Wait Until Page Contains Element	xpath=//html/body/div[4]/a
    Click Element                       xpath=//html/body/div[4]/a
    Wait Until Page Contains            Como podemos ajudar?
Access News
	Wait Until Page Contains Element	xpath=//html/body/div[17]/div/div[1]/div/a[1]
	Click Element                       xpath=//html/body/div[17]/div/div[1]/div/a[1]
    Wait Until Page Contains            1. Novo resumo
News Validate
    Wait Until Page Contains            Criamos um novo resumo para suas campanhas em andamento, para garantir total transparência e facilitar sua análise.
    Wait Until Page Contains Element    xpath=/html/body/div[18]/div/div[5]/a[3]      
    Click Element                       xpath=/html/body/div[18]/div/div[5]/a[3]
    Wait Until Page Contains            2. Ícone ajuda
    Page Should Contain                 Estamos implementando um sistema de ajuda e tutoriais, sempre que vir este ícone 
    Page Should Contain                  em uma tela você terá acesso a novidades e tutorial  dessa tela.    
    Wait Until Page Contains Element    xpath=//html/body/div[18]/div/div[5]/a[3]
    Click Element                       xpath=//html/body/div[18]/div/div[5]/a[3]
    Wait Until Page Contains            3. Tutorial
    Page Should Contain                 Aproveite e confira o tutorial dessa tela!
Access Tutorial
    Wait Until Page Contains Element    xpath=//html/body/div[17]/div/div[1]/div/a[2]
    Click Element                       xpath=//html/body/div[17]/div/div[1]/div/a[2]
    Wait Until Page Contains            1. Importados
Tutorial Validate
    Wait Until Page Contains            Quando uma campanha é criada, seja por Painel, API ou Integração FTP, o sistema faz uma higienização dos registros solicitados, removendo os inválidos, repetidos e bloqueados pela blacklist.
    Wait Until Page Contains            Os demais registros são processados para envio.
    Wait Until Page Contains Element    xpath=//html/body/div[17]/div/div[5]/a[3]
    Click Element                       xpath=//html/body/div[17]/div/div[5]/a[3]
    Wait Until Page Contains            2. Cancelados
    Wait Until Page Contains            Os registros que foram processados passam por um filtro no momento do envio, nesse filtro existem os bloqueios por regra de negócio previamente cadastrados. Os registros que forem identificados nessa análise têm seus envios cancelados. O cancelamento pode vir também por ação do usuário.
    Wait Until Page Contains Element    xpath=//html/body/div[17]/div/div[5]/a[3]
    Click Element                       xpath=//html/body/div[17]/div/div[5]/a[3]
    Wait Until Page Contains            3. Aguardando Envio
    Wait Until Page Contains            Os registros que não forem cancelados irão aguardar o envio de acordo com a data e hora agendada no momento da criação da campanha, lembrando que podem existir regras de horários cadastrados no perfil da carteira/cliente.
    Wait Until Page Contains Element    xpath=//html/body/div[17]/div/div[5]/a[3]
    Click Element                       xpath=//html/body/div[17]/div/div[5]/a[3]           
    Wait Until Page Contains            4. Envios
    Wait Until Page Contains            Você tem acesso ao andamento dos envios em tempo real.
    Wait Until Page Contains Element    xpath=//html/body/div[17]/div/div[5]/a[3]
    Click Element                       xpath=//html/body/div[17]/div/div[5]/a[3]    
    Wait Until Page Contains            5. Retorno da operadora
    Wait Until Page Contains            As mensagens são enviadas para a operadora e cabe a ela retornar a informação se a mensagem foi entregue ou não entregue ao destinatário. Essa confirmação pode levar até 72h para ser atualizada. Enquanto não tivermos a confirmação da operadora, a mensagem que foi enviada fica com o status aguardando retorno.
    Capture Page Screenshot
    Wait Until Page Contains Element    xpath=//html/body/div[17]/div/div[5]/a[1]
    Click Element                       xpath=//html/body/div[17]/div/div[5]/a[1]
    Wait Until Page Contains            Campanhas em andamento



                          