*** Settings ***
Resource        ../basesms.robot
Test Setup      Nova sessão
Test Teardown   Encerra Sessão

*** Variables ***


*** Test Cases ***
Teste
    [tags]     Teste
    ${user_name}=   Generate Random String  5   [LETTERS]
    log to console  ${user_name}


New User
    Login With
    Select Product and Company
    Registration Access
    New User


*** Keywords ***

Registration Access
    Sleep                                   2
    Wait Until Page Contains Element        id=aba6
    Mouse Over                              id=aba6
    Wait Until Page Contains Element        xpath=//html/body/div[11]/ul/li[2]/a
    Click Element                           xpath=//html/body/div[11]/ul/li[2]/a
    Wait Until Page Contains                Usuários / Permissões
New User
    ${user_name}=   Generate Random String  5   [LETTERS]
    Wait Until Page Contains Element        class:main
    Click Element                           class:main
    Wait Until Page Contains                Usuários
    Wait Until Page Contains Element        id=nome    
    Input Text                              id=nome     ${user_name}
    Sleep                                   10



