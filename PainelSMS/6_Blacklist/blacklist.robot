*** Settings ***
Resource        ../basesms.robot
Test Setup      Nova sessão     SMS
Test Teardown   Encerra Sessão

*** Variables ***
${login}        teste@sms.com
${password}     Pgmais123

#LOCATORS BLACKLIST
${CADASTRAR}   xpath=//span[contains(text(),"Cadastrar Número")]/..
${TELEFONE}    id=telefone
${FILTRAR}     id=filtrar
${DT_INICIO}   id=dataInicio
${DT_FIM}      id=dataFim
${checkAll}    id=checkAll
${EXCLUIR}     id=bulkRemoveButton

#LOCATORS INSERÇAO DE NÚMEROS
${SELECT_EMPRESA}     xpath=//*[@title="Nenhum Selecionado"]/../select
${INPUT_TELEFONE}    id=list_telefone
${INCLUIR}     xpath=//input[@value="Incluir Telefones"]

*** Test Cases ***
Cenário 01: Validação de inserção de número na inválido na blacklist
    Dado que estou na tela de blacklist
    Quando clicar em "Cadastrar Número"
    Então deve ser exibida a tela "Campanhas"

    Quando preencher o formulário com número inválido e incluir telefones
    Então deve ser exibido uma mensagem de número inválido

Cenário 02: Validação de inserção de números repetidos na blacklist
    Dado que estou na tela de blacklist
    Quando clicar em "Cadastrar Número"
    Então deve ser exibida a tela "Campanhas"

    Quando preencher o formulário com número repetido e incluir telefones
    Então deve ser exibido uma mensagem de número repetido

Cenário 03: Validação de inserção de número válido na blacklist
    Dado que estou na tela de blacklist
    Quando clicar em "Cadastrar Número"
    Então deve ser exibida a tela "Campanhas"

    Quando preencher o formulário com número válido e incluir telefones
    Então deve ser exibido uma mensagem de número inserido

Cenário 04: Validação de filtro com telefone inválido
    Dado que estou na tela de blacklist
    Quando informar um telefone inválido para filtro
    Então deve ser exibido uma mensagem de validação sobre o telefone

Cenário 05: Validação de filtro com data inicial inválida
    Dado que estou na tela de blacklist
    Quando informar data início inválida para filtro
    Então deve ser exibido uma mensagem de validação sobre data início

Cenário 06: Validação de filtro com data final inválida
    Dado que estou na tela de blacklist
    Quando informar data fim inválida para filtro
    Então deve ser exibido uma mensagem de validação sobre data fim

Cenário 07: Validação de exclusão individual
    Dado que estou na tela de blacklist
    Quando clicar na lixeia para excluir 
    Então deve ser exibido uma janela de confirmação

    Quando clicar em Excluir

Cenário 09: Validação de exclusão em lote
    Dado que estou na tela de blacklist
    Quando marcar o check geral e clicar em excluir selecionados
    Então deve ser exibido uma janela de confirmação

    Quando clicar em Excluir

*** Keywords ***
Dado que estou na tela de blacklist
    Acessar painel
    Acessar menus  Jobs  Black List

Quando marcar o check geral e clicar em excluir selecionados
     Wait Until Element Is Visible  xpath=//*[@id='tabela-avulso']//tr[1]//td[6]/a    10s
     Click Element  ${checkAll}
     Click Element  ${EXCLUIR}

Quando clicar em "${ELEMENTO}"
    Run Keyword If  '${ELEMENTO}'=='Cadastrar Número'      Click Element  ${CADASTRAR}

Quando preencher o formulário com número inválido e incluir telefones
    Select From List By Label  ${SELECT_EMPRESA}  rota5000
    Input Text  ${INPUT_TELEFONE}     41999999999
    Click Element    ${INCLUIR}

Quando preencher o formulário com número repetido e incluir telefones
    Select From List By Label  ${SELECT_EMPRESA}  rota5000
    Input Text  ${INPUT_TELEFONE}     41999991111, 41999991111, 41999992222
    Click Element    ${INCLUIR}

Quando preencher o formulário com número válido e incluir telefones
    Select From List By Label  ${SELECT_EMPRESA}  rota5000
    Input Text                 ${INPUT_TELEFONE}        43996505654
    Click Element              ${INCLUIR}   

Quando informar um telefone inválido para filtro
    Wait Until Element Is Visible  ${TELEFONE} 
    Input Text                 ${TELEFONE}        41
    Click Element              ${FILTRAR}  

Quando informar data início inválida para filtro
    Wait Until Element Is Visible  ${DT_INICIO} 
    Input Text                 ${DT_INICIO}        1
    Click Element              ${FILTRAR} 

Quando informar data fim inválida para filtro
    Wait Until Element Is Visible  ${DT_FIM} 
    Input Text                 ${DT_FIM}        1
    Click Element              ${FILTRAR} 

Quando clicar na lixeia para excluir 
     Wait Until Element Is Visible  xpath=//*[@id='tabela-avulso']//tr[1]//td[6]/a    10s
     Click Element  xpath=//*[@id='tabela-avulso']//tr[1]//td[6]/a

Quando clicar em Excluir
    Wait Until Element Is Visible  xpath=//*[contains(text(),"EXCLUIR")]
    Click Element                  xpath=//*[contains(text(),"EXCLUIR")]

Então deve ser exibida a mensagem de sucesso
    Element Should Be Visible  xpath=//span[contains(text(),"Os itens foram deletados da Blacklist")]

Então deve ser exibido uma mensagem de validação sobre data fim
    Page Should Contain  A Data precisa ser válida e no formato DD/MM/YYYY 

Então deve ser exibido uma mensagem de validação sobre data início
    Page Should Contain  A Data precisa ser válida e no formato DD/MM/YYYY  

Então deve ser exibido uma mensagem de validação sobre o telefone
    Page Should Contain        Telefone Possui quantidade de digitos inv?lida

Então deve ser exibido uma mensagem de número inválido
    Wait Until Element Is Visible  xpath=//p[contains(text(), "Telefones informados")]   
    ${HTML}=                       Get Text  xpath=//p[contains(text(), "Telefones informados")]   
    Should Contain  ${HTML}        Inválidos: 1

Então deve ser exibido uma mensagem de número repetido
    Wait Until Element Is Visible  xpath=//p[contains(text(), "Telefones informados")]   
    ${HTML}=                       Get Text  xpath=//p[contains(text(), "Telefones informados")]   
    Should Contain  ${HTML}        Repetidos: 1

Então deve ser exibida a tela "${TELA}"
    Wait Until Element Is Visible    xpath=//h5[contains(text(), "${TELA}")]

Então deve ser exibido uma mensagem de número inserido
    Wait Until Element Is Visible  xpath=//p[contains(text(), "Telefones informados")]   
    ${HTML}=                       Get Text  xpath=//p[contains(text(), "Telefones informados")]   
    Should Contain  ${HTML}        Importados: 1

Então deve ser exibido uma janela de confirmação
    Page Should Contain            Tem certeza que deseja excluir