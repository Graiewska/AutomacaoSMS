*** Settings ***
Library               SeleniumLibrary
Library               String
Library               OperatingSystem
Library               FakerLibrary
Library               DateTime
Library               SoapLibrary
Library               XML
Library               String
Library               Collections
Library               RequestsLibrary  



*** Variables  ***
#Painel
${url_painel}                  http://painel.maisresultado.com.br/
${url_sms}                     http://sms.maisresultado.com.br/
${browser}              headlessChrome 
${full_name}            Teste Sms
${empresa}              QA Automacao


######## MÉTODOS PARA PAINEL #############
*** Keywords ***
Nova sessão
    [Arguments]                     ${url}                          
    Run Keyword If                  '${url}'=='PAINEL'      Run Keywords        Open Browser        ${url_painel}       ${browser}   options=add_experimental_option('excludeSwitches', ['enable-logging'])    AND     Maximize Browser Window
    ...     ELSE IF                 '${url}'=='SMS'         Run Keywords        Open Browser        ${url_sms}          ${browser}   options=add_experimental_option('excludeSwitches', ['enable-logging'])    AND     Maximize Browser Window
Encerra Sessão
    Sleep                           5
    Close Browser

Dado que estou no login unico dos paineis
    [Arguments]                         ${login}       ${password} 
    Input Text                          css:input[name=username]      ${login}
    Input Text                          css:input[name=password]      ${password}

Dado que estou no login de SMS
    [Arguments]                         ${login}       ${password} 
    Input Text                          css:input[name=username]      ${login}
    Input Text                          css:input[name=password]      ${password}

Quando clicar em "entrar"
    Click Element                       class:button-submit-login   

Então deve ser exibida a tela de "seleção de empresas"    
    [Arguments]                          ${full_name}       ${empresa}
    Wait Until Element is Visible        id=carteiraSelect        15s
    Click Element  id=carteiraSelect
    Sleep  3s
    Input Text  xpath=//div[@class='bs-searchbox']/input  ${empresa}
    Sleep  3s
    Press Keys    xpath=//div[@class='bs-searchbox']/input    ENTER

Quando clicar em "confirmar"    
    Click Element                        xpath=//button[contains(text(),'Confirmar')]

Então o Sistema deverá exibir a tela "Dashboard"
    Wait Until Page Contains Element        xpath=//*[@id="logo"]       60

Dado que estou no login unico dos paineis com o usuario externo
    [Arguments]                         ${login}       ${password} 
    Input Text                          css:input[name=username]      ${login}
    Input Text                          css:input[name=password]      ${password}

Obter data atual
    ${DATA}=          Get Current Date
    [Return]          ${DATA}
Acrescentar dias à data atual
    [Arguments]         ${DIAS}
    ${DATA}=            Obter data atual
    ${DATA}=            Add Time To Date    ${DATA}    ${DIAS}
    ${DATA}=            Convert Date        ${DATA}    result_format=%d/%m/%Y
    [Return]            ${DATA}
 

    #### LOGIN SIMPLIPLIFICADO ####
Realizar login
    [Arguments]                         ${username}                                                    ${senha}
    Input Text                          ${CAMPO_EMAIL}                                                       ${username}
    Input Text                          ${CAMPO_SENHA}                                                       ${senha}
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Element Is Visible       id=login_company                                                   10s
Selecionar empresa
    Select From List By Label           ${COMBO_EMPRESAS}                                                    ${empresa}
    Click Element                       ${BOTAO_CONFIRMAR}
    Wait Until Page Contains Element    id=icone_usuario                                                     10 seconds
Acessar painel
    Realizar login                      ${username}                                                    ${senha}
    Selecionar empresa
Acessar menus
    [Arguments]                         ${ABA}                                                               ${OPCAO}
    Mouse Down On Link                  xpath=//div[@id='menu']//a[contains(text(), '${ABA}')]
    Click Element                       xpath=//div[@class='anylinkmenu']//a[contains(text(),'${OPCAO}')]