*** Settings ***
Library               SeleniumLibrary
Library               String
Library               OperatingSystem
Library               FakerLibrary
Library               DateTime
Library               XML
Library               String
Library               Collections
 
*** Variables  ***
${URL_PAINEL}           http://painel.maisresultado.com.br/
${URL_SMS}              http://sms.maisresultado.com.br/
${BROWSER}              Chrome 

#TELA DE LOGIN
${CAMPO_EMAIL}          id=username
${CAMPO_SENHA}          id=password
${BOTAO_ENTRAR}         xpath=//*[@id='login_form']//button

#TELA DE SELEÇÃO DE EMPRESAS
${SELECT_EMPRESAS}      xpath=//*[@class='form-group selectpicker']
${BOTAO_CONFIRMAR}      xpath=//*[@id='login_company']//button[contains(text(), 'Confirmar')]

&{LOGIN}                username=teste@sms.com  
...                     username_beta=testehomolog@sms.com
...                     externo=qaexterno@sms.com   
...                     senha=Pgmais123    
...                     empresa=QA PgMais
# ...                     empresa=IM Cobranças

${TIMEOUT}              60s                 #TIMEOUT PADRAO PARA TODOS WAIT

*** Keywords ***
Abrir navegador
    Open Browser                   about:blank    ${BROWSER}  options=add_experimental_option('excludeSwitches', ['enable-logging'])
    Maximize Browser Window  

Fechar navegador
    Capture Page Screenshot  filename=teste.png
    Close Browser

Navegar até a dashboard
    Go To   ${URL_SMS}

Realizar login
    [Arguments]                         ${LOGIN.username_beta}  ${LOGIN.senha}
    Go To                               ${URL_SMS}
    Input Text                          ${CAMPO_EMAIL}          ${LOGIN.username_beta}
    Input Text                          ${CAMPO_SENHA}          ${LOGIN.senha}
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Element Is Visible       id=login_company                                                   10s

Selecionar empresa
    Select From List By Label           ${SELECT_EMPRESAS}      ${LOGIN.empresa}
    Click Element                       ${BOTAO_CONFIRMAR}
    Wait Until Page Contains Element    id=icone_usuario                                                     10 seconds

Acessar painel
    Realizar login                      ${LOGIN.username}  ${LOGIN.senha}
    Selecionar empresa

Acessar menus
    [Arguments]                         ${ABA}                                                               ${OPCAO}
    Mouse Down On Link                  xpath=//div[@id='menu']//a[contains(text(), '${ABA}')]
    Click Element                       xpath=//div[@class='anylinkmenu']//a[contains(text(),'${OPCAO}')]

Realizar logout
    Click Element                   id=icone_usuario
    Wait Until Element Is Visible   xpath=//*[@id='menu_pessoal']
    Click Element                   xpath=//a[contains(text(), 'Sair')]
    Page Should Contain             Bem vindo ao painel SMS!

Obter data atual
    ${DATA}=          Get Current Date
    [Return]          ${DATA}

Acrescentar dias à data atual
    [Arguments]         ${DIAS}
    ${DATA}=            Obter data atual
    ${DATA}=            Add Time To Date    ${DATA}    ${DIAS} days
    ${DATA}=            Convert Date        ${DATA}    result_format=%d/%m/%Y
    [Return]            ${DATA}
 
Dado que estou logado no painel SMS
    Acessar painel