*** Settings ***
Resource               ../Resource/Resource.robot
Suite SETUP             Abrir navegador
Suite TEARDOWN          Fechar navegador

*** Variable ***
#Locators Menu usuário
${ICONE_USUARIO}       id=icone_usuario
${LINK_SAIR}           xpath=//a[contains(text(), 'Sair')]
${LINK_CONFIG}         xpath=//a[contains(text(), 'Configurações')]
${LINK_MINHA_CONTA}    xpath=//a[contains(text(), 'Minha Conta')]

*** Test Case ***
### Iterações com botão de usuário
Validação de acesso à página de configurações pelo menu de usuário
    [tags]    Regressao1
    Dado que estou no painel de email
    Quando clicar em "Configurações"
    Então deve ser exibida a página "Configurações"

Acessar página Minha Conta pelo menu de usuário
    [tags]    Regressao1
    Navegar até a dashboard
    Quando clicar em "Minha Conta"
    Então deve ser exibida a página "Minha Conta"

### Iterações com menu CADASTROS
Validação de acesso à página clientes
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Clientes"
    Então deve ser exibida a página "Clientes"

Validação de acesso à página usuários
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Usuários"
    Então deve ser exibida a página "Usuários"

Validação de acesso à página Alertas Jobs
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Alertas Jobs"
    Então deve ser exibida a página "Alertas Jobs"

Validação de acesso à página Resposta radar
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Resposta Radar"
    Então deve ser exibida a página "Gerenciar Mensagens de Retorno Radar"

Validação de acesso à página Minha Conta
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Minha Conta"
    Então deve ser exibida a página "Minha Conta"

Validação de acesso à página Configurações
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Configurações"
    Então deve ser exibida a página "Configurações"

Validação de acesso à página Orçamento
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Orçamento"
    Então deve ser exibida a página "Orçamento"

Validação de acesso à página de gerenciamento por IP
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Cadastros" e clicar em "Bloqueio por IP"
    Então deve ser exibida a página "Gerenciar IP"

## Iterações com menu RELATÓRIOS
Validação de acesso à página de relatório sintético
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Sintético"
    Então deve ser exibida a página "Sintético"

Validação de acesso à página de relatório Analítico
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Analítico"
    Então deve ser exibida a página "Analítico"

Validação de acesso à página de relatório bloqueados
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Bloqueados"
    Então deve ser exibida a página "Bloqueios"

Validação de acesso à página de relatório telefones operadoras
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Telefones - Operadoras"
    Então deve ser exibida a página "Telefones - Operadoras"

Validação de acesso à página de relatório Blacklist
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Blacklist"
    Então deve ser exibida a página "BlackList"

Validação de acesso à página de relatório Exportação Personalizada
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Exportação Personalizada"
    Então deve ser exibida a página "Exportação Personalizada"

Validação de acesso à página de relatório Extrato de Movimentação
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em Extrato de Movimentação
    Então deve ser exibida a página "Extrato de Movimentação"

Validação de acesso à página de relatório Histórico de Ações
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Histórico de Ações"
    Então deve ser exibida a página "Histórico de Ações"

Validação de acesso à página de relatório Avulsos
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Envios Avulsos"
    Então deve ser exibida a página "Envios Avulsos"

Validação de acesso à página de relatório Envios API
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Envios API"
    Então deve ser exibida a página "Envios API"

Validação de acesso à página de relatório Por Telefone
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Por Telefone"
    Então deve ser exibida a página "Por Telefone"

Validação de acesso à página de Relatório Mensagens
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em Mensagens
    Então deve ser exibida a página "Mensagens"

Validação de acesso à página de Relatório Interativo
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em Interativo
    Então deve ser exibida a página "Interativo"

Validação de acesso à página de Relatório Consulta por telefone
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Relatórios" e clicar em "Consulta por telefone"
    Então deve ser exibida a página "Aqui você pode pesquisar todas as interações de um determinado telefone"

### Itenações com menu JOBS
Validação de acesso à página Jobs/Gerenciar
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Gerenciar"
    Então deve ser exibida a página "Esse é o histórico de suas campanhas."

Validação de acesso à página Jobs/Avulsos API
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Avulsos/API"
    Então deve ser exibida a página "Aqui você pode pesquisar todos os envios avulsos."

Validação de acesso à página Jobs/Importar
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Importar"
    Então deve ser exibida a página "Criação da Mensagem"

Validação de acesso à página Jobs/Extrato de Movimentação
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Extrato de Movimentação"
    Então deve ser exibida a página "Extrato Movimentação"

Validação de acesso à página Jobs/Black List
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Black List"
    Então deve ser exibida a página "Gerenciar Blacklist"

Validação de acesso à página Jobs/White List
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "White List"
    Então deve ser exibida a página "Gerenciar Whitelist"

Validação de acesso à página Jobs/Black List Campo Informado
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Black List Campo Informado"
    Então deve ser exibida a página "Gerenciar Blacklist de Campo Informado"

Validação de acesso à página Jobs/Templates de Mensagem
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Template de Mensagem"
    Então deve ser exibida a página "Listagem de Templates de Mensagens"

Validação de acesso à página Jobs/Template Concatenado
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Template Concatenado"
    Então deve ser exibida a página "Listagem de Templates Concatenados"

Validação de acesso à página Jobs/Template Interativo
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Template Interativo"
    Então deve ser exibida a página "Listagem de Templates Interativos"

Validação de acesso à página Jobs/Reprocessar Jobs
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Reprocessar Jobs"
    Então deve ser exibida a página "Reprocessamento de Campanhas"

Validação de acesso à página Jobs/Envio rápido concatenado
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Envio rápido concatenado"
    Então deve ser exibida a página "Envio Rápido Concatenado"

Validação de acesso à página Jobs/Envio Rapido
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Envio Rápido"
    Então deve ser exibida a página "Envio Rápido"

Validação de acesso à página Jobs/Radar
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Radar"
    Então deve ser exibida a página "Retornos"

### Itenações com menu Corporativo
Validação de acesso à página Corporativo - Grupos
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Corporativo" e clicar em "Corporativo - Grupos"
    Então deve ser exibida a página "Grupos"

Validação de acesso à página Corporativo - Contatos
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Corporativo" e clicar em "Corporativo - Contatos"
    Então deve ser exibida a página "Contatos"    

Validação de acesso à página Corporativo - campanha
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Corporativo" e clicar em "Enviar Mensagens"
    Então deve ser exibida a página "conteúdo da Campanha"  

Validação de acesso à página Corporativo - gerenciar campanha
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Corporativo" e clicar em "Corp. Gerenciar Campanha"
    Então deve ser exibida a página "Corporativo - Gerenciar Campanhas" 

Validação de logout do painel
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Sair"
    Então deve voltar para tela de login

*** Keywords ***
Dado que estou no painel de email
    Acessar painel

Quando clicar em "${LINK}"
    Click Element                   ${ICONE_USUARIO}
    Wait Until Element Is Visible   xpath=//*[@id='menu_pessoal']       ${TIMEOUT}
    Run Keyword If                  '${LINK}'=='Sair'                   Click Element   ${LINK_SAIR}
    ...    ELSE IF                  '${LINK}'=='Configurações'          Click Element   ${LINK_CONFIG}
    ...    ELSE IF                  '${LINK}'=='Minha Conta'            Click Element   ${LINK_MINHA_CONTA}

Quando ir em "${ABA}" e clicar em "${OPCAO}"
    Acessar menus   ${ABA}  ${OPCAO}

Quando ir em "${ABA}" e clicar em Extrato de Movimentação
    Mouse Down On Link  xpath=//div[@id='menu']//a[contains(text(), '${ABA}')]
    Click Element       xpath=//div[@class='anylinkmenu']//a[@href='/rel-movimentacao.php']

Quando ir em "${ABA}" e clicar em Mensagens
    Mouse Down On Link  xpath=//div[@id='menu']//a[contains(text(), '${ABA}')]
    Click Element       xpath=//div[@class='anylinkmenu']//a[@href='/rel-mensagens.php']

Quando ir em "${ABA}" e clicar em Interativo
    Mouse Down On Link  xpath=//div[@id='menu']//a[contains(text(), '${ABA}')]
    Click Element       xpath=//div[@class='anylinkmenu']//a[@href='/rel-interativo.php']

Então deve voltar para tela de login
    Page Should Contain     Bem vindo ao painel SMS!

Então deve ser exibida a página "${PAGINA}"
    Run Keyword If      '${PAGINA}'=='Gerenciar Campanhas'                      Page Should Contain Element     xpath=//div[@class='menu-row-titulo mb-3 w-100']//p[contains(text(),'${PAGINA}')]
    ...    ELSE IF      '${PAGINA}'=='Esse é o histórico de suas campanhas.'    Page Should Contain Element     xpath=//div[@class='menu-row-titulo mb-3 w-100']//p[contains(text(),'${PAGINA}')]
    ...    ELSE IF      '${PAGINA}'=='Corporativo - Gerenciar Campanhas'        Page Should Contain Element     xpath=//div[@class='menu-row-titulo']/p[contains(text(),'Gerenciar Campanhas')]
    ...       ELSE      Page Should Contain Element                             xpath=//div[@class='menu-row-titulo']/p[contains(text(),'${PAGINA}')]




    