*** Settings ***
Resource                           ../Resource/Resource.robot
Suite SETUP                         Abrir navegador
Suite TEARDOWN                      Fechar navegador

*** Variable ***
# DASHBBOARD
${BOTAO_NOVA_CAMPANHA}             xpath=//span[contains(text(),"Nova Campanha")]/..

${CARTEIRA}                        Teste API APPS
${TEMPLATE}                        rcs_text
${ARQUIVO.TXT}                     ${CURDIR}//Files//Campanha.txt
${ARQUIVO.CSV}                     ${CURDIR}//Files//Campanha.csv
${ARQUIVO_INV}                     ${CURDIR}//Files//Números inválidos.txt
${ARQUIVO_BL}                      ${CURDIR}//Files//Números blacklist.txt

# CRIAR CAMPANHA
${CANAL_RCS}                       xpath=//span[contains(text(),'RCS')]/../../button
${SELECT_CARTEIRA}                 id=id_empresa
${SELECT_TEMPLATE}                 id=id_template
${BUSCAR_ARQUIVOS}                 id=upload-files
${BLOCO_TOTAIS}                    id=bloco-totais
${INFO_MESSAGE}                    id=info-vue-message
${CRIAR_CAMPANHA}                  id=submit-buttom
${MODAL_ACOMPANHAMENTO}            id=model-content-acompanhamento
${RADIO_AGENDAR_DISPARO}           id=tipo_disparo_agendar
${INPUT_DATA_AGENDA}               xpath=//*[@data-id='data_agendamento']
${INPUT_HORA_AGENDA}               xpath=//*[@data-id='hora_agendamento']
${LIMPAR_TUDO}                     id=limpar-tudo

# MODAL TEMPLATE RCS_TEXT
${SELECT_FONE}                     id=select_numero_telefone
${SELECT_NOME}                     id=select_nome
${SELECT_TIME}                     id=select_time
${SALVAR_CONFIG}                   id=botao_select

# MODAL CONFIRMAÇÂO DE ENVIO
${MODAL_CONFIRM}                   id=model-content-confirmacao
${CONFIRMAR_ENVIO}                 xpath=//button[contains(text(),"Confirmar o envio")]

*** Test Cases ***
Validação de Criação de campanhas com arquivo txt
    Dado que estou logado no painel SMS
    Acessar nova dash - (retirar após arrumarem)
    Quando clicar em "Nova Campanha"
    Então deverá ser exibida a tela "Criação de campanha"

    Quando clicar em "RCS"
    Então deverá ser exibido uma informação sobre o produto

    Quando informar uma carteira
    Então deverá ser exibido as informações da Empresa

    Quando selecionar o template e o "arquivo.txt"
    Então deverá ser exibido o modal de configuração de arquivo

    Quando configurar o arquivo
    Então deverá ser exibido os totais e a prévia na tela criar campanhas
    
    Quando clicar em "Criar Campanha"
    Então deverá ser exibido a modal de confirmação de dados

    Quando clicar em "Confirmar o envio"
    Então deve ser exibido o modal de acompanhamento

Validação de Criação de campanhas com arquivo csv
    Dado que estou logado no painel SMS
    Acessar nova dash - (retirar após arrumarem)
    Quando clicar em "Nova Campanha"
    Então deverá ser exibida a tela "Criação de campanha"

    Quando clicar em "RCS"
    Então deverá ser exibido uma informação sobre o produto

    Quando informar uma carteira
    Então deverá ser exibido as informações da Empresa

    Quando selecionar o template e o "arquivo.csv"
    Então deverá ser exibido o modal de configuração de arquivo

    Quando configurar o arquivo
    Então deverá ser exibido os totais e a prévia na tela criar campanhas
    
    Quando clicar em "Criar Campanha"
    Então deverá ser exibido a modal de confirmação de dados

    Quando clicar em "Confirmar o envio"
    Então deve ser exibido o modal de acompanhamento

Validação de Criação de campanhas com números inválidos
    Dado que estou logado no painel SMS
    Acessar nova dash - (retirar após arrumarem)
    Quando clicar em "Nova Campanha"
    Então deverá ser exibida a tela "Criação de campanha"

    Quando clicar em "RCS"
    Então deverá ser exibido uma informação sobre o produto

    Quando informar uma carteira
    Então deverá ser exibido as informações da Empresa

    Quando selecionar o template e o "arquivo com números inválidos"
    Então deverá ser exibido o modal de configuração de arquivo

    Quando configurar o arquivo
    Então deverá ser exibido os números inválidos
    
Validação de Criação de campanhas com números blacklist
    [Tags]      teste
    Dado que estou logado no painel SMS
    Acessar nova dash - (retirar após arrumarem)
    Quando clicar em "Nova Campanha"
    Então deverá ser exibida a tela "Criação de campanha"

    Quando clicar em "RCS"
    Então deverá ser exibido uma informação sobre o produto

    Quando informar uma carteira
    Então deverá ser exibido as informações da Empresa

    Quando selecionar o template e o "arquivo com números blacklist"
    Então deverá ser exibido o modal de configuração de arquivo

    Quando configurar o arquivo
    Então deverá ser exibido os números blacklist

Validação de envio de campanha agendando disparo
    Dado que estou logado no painel SMS
    Acessar nova dash - (retirar após arrumarem)
    Quando clicar em "Nova Campanha"
    Então deverá ser exibida a tela "Criação de campanha"

    Quando clicar em "RCS"
    Então deverá ser exibido uma informação sobre o produto

    Quando informar uma carteira
    Então deverá ser exibido as informações da Empresa

    Quando selecionar o template e o "arquivo.txt"
    Então deverá ser exibido o modal de configuração de arquivo

    Quando configurar o arquivo
    Então deverá ser exibido os totais e a prévia na tela criar campanhas
    
    Quando selecionar a opção "Agendar Disparo"
    Então deverá ser exibido os campos de configuração

    Quando preencher os campos com informações válidas
    Quando clicar em "Criar Campanha"
    Então deverá ser exibido a modal de confirmação de dados com o horário agendado
    
    Quando clicar em "Confirmar o envio"
    Então deve ser exibido o modal de acompanhamento

Validação da função Limpar tudo
    [Tags]    novo
    Dado que estou logado no painel SMS
    Acessar nova dash - (retirar após arrumarem)
    Quando clicar em "Nova Campanha"
    Então deverá ser exibida a tela "Criação de campanha"

    Quando clicar em "RCS"
    Então deverá ser exibido uma informação sobre o produto

    Quando informar uma carteira
    Então deverá ser exibido as informações da Empresa

    Quando selecionar o template e o "arquivo.txt"
    Então deverá ser exibido o modal de configuração de arquivo

    Quando configurar o arquivo
    Então deverá ser exibido os totais e a prévia na tela criar campanhas
    
    Quando clicar em "Limpar tudo"
    Então o formulário deverá ser reiniciado

*** Keywords ***
Acessar nova dash - (retirar após arrumarem)
    # Keyword temporário até deixarem fixo o acesso a nova Dash
    Go To  https://sms.maisresultado.com.br/Dashboard
    Wait Until Element Is Visible  xpath=//*[@class="introjs-tooltip introjs-floating"]  ${TIMEOUT}
    Click Element  xpath=//*[@class="introjs-tooltip introjs-floating"]//a[contains(text(),"Sair")]

Quando clicar em "${ELEMENTO}"
    # Run Keyword If  '${ELEMENTO}'=='Nova Campanha'    Click Element   ${BOTAO_NOVA_CAMPANHA}
    Run Keyword If  '${ELEMENTO}'=='Nova Campanha'      Go To   https://sms.maisresultado.com.br/CriarCampanha
    ...    ELSE IF  '${ELEMENTO}'=='RCS'                Click Element   ${CANAL_RCS}
    ...    ELSE IF  '${ELEMENTO}'=='Criar Campanha'     Click Element   ${CRIAR_CAMPANHA}
    ...    ELSE IF  '${ELEMENTO}'=='Confirmar o envio'  Click Element   ${CONFIRMAR_ENVIO}
    ...    ELSE IF  '${ELEMENTO}'=='Limpar tudo'        Click Element   ${LIMPAR_TUDO}

Quando preencher os campos com informações válidas
    ${DATA}=                        Acrescentar dias à data atual    1
    Set Global Variable             ${DATA}
    Input Text                      ${INPUT_DATA_AGENDA}    ${DATA}
    Click Element                   ${INPUT_HORA_AGENDA}
    Wait Until Element Is Visible   xpath=//*[@class='dhx_popup-content']  ${TIMEOUT}
    Wait Until Element Is Visible   xpath=//*[@class='dhx_timepicker-input dhx_timepicker-input--hour']   ${TIMEOUT}
    Wait Until Element Is Visible   xpath=//*[@class='dhx_timepicker-input dhx_timepicker-input--minutes']  ${TIMEOUT}
    Input Text                      xpath=//*[@class='dhx_timepicker-input dhx_timepicker-input--hour']      12    clear=true
    Clear Element Text              xpath=//*[@class='dhx_timepicker-input dhx_timepicker-input--minutes'] 
    Capture Page Screenshot         filename=selenium-screenshot.png

Quando informar uma carteira
    Select From List By Label  ${SELECT_CARTEIRA}  ${CARTEIRA}

Quando selecionar a opção "${ELEMENTO}"
    Run Keyword If  '${ELEMENTO}'=='Agendar Disparo'    Select Radio Button  tipo_disparo    agendar

Quando selecionar o template e o "${TIPO_ARQUIVO}"
    Select From List By Label    ${SELECT_TEMPLATE}  ${TEMPLATE}
    Run Keyword If  '${TIPO_ARQUIVO}'=='arquivo.txt'                      Choose File   ${BUSCAR_ARQUIVOS}  ${ARQUIVO.TXT}
    ...    ELSE IF  '${TIPO_ARQUIVO}'=='arquivo.csv'                      Choose File   ${BUSCAR_ARQUIVOS}  ${ARQUIVO.CSV}
    ...    ELSE IF  '${TIPO_ARQUIVO}'=='arquivo com números inválidos'    Choose File   ${BUSCAR_ARQUIVOS}  ${ARQUIVO_INV}
    ...    ELSE IF  '${TIPO_ARQUIVO}'=='arquivo com números blacklist'    Choose File   ${BUSCAR_ARQUIVOS}  ${ARQUIVO_BL}

Quando configurar o arquivo
    Select From List By Value       ${SELECT_FONE}  coluna0
    Select From List By Value       ${SELECT_NOME}  coluna1
    Select From List By Value       ${SELECT_TIME}  coluna2
    Click Element                   ${SALVAR_CONFIG}

Então o formulário deverá ser reiniciado
    Element Should Not Be Visible       id=info-produto-container
    Element Should Not Be Visible       id=info-empresa
    Element Should Not Be Visible       ${BLOCO_TOTAIS}
    Element Should Not Be Visible       xpath=//*[@class='file-list-item p-1 col-11 pointer bg-white border-success']

Então deve ser exibido o modal de acompanhamento
    Wait Until Element Is Visible  ${MODAL_ACOMPANHAMENTO}  ${TIMEOUT}
    ${ID_CAMPANHA}=                Get WebElement         xpath=//*[@data-id='id_campanha']
    Log To Console                 A campanha criada foi: ${ID_CAMPANHA.text}
    Log                            A campanha criada foi: ${ID_CAMPANHA.text}

Então deverá ser exibido os campos de configuração
    Wait Until Element Is Visible       ${INPUT_DATA_AGENDA}  ${TIMEOUT}
    Wait Until Element Is Visible       ${INPUT_HORA_AGENDA}  ${TIMEOUT}

Então deverá ser exibido a modal de confirmação de dados
    Wait Until Element Is Visible  ${MODAL_CONFIRM}     ${TIMEOUT}
    Wait Until Element Is Visible  ${CONFIRMAR_ENVIO}   ${TIMEOUT}
    Sleep  2s  reason=Botão confirmar envio precisa de um tempo para se tornar clicável

Então deverá ser exibido a modal de confirmação de dados com o horário agendado
    Wait Until Element Is Visible  ${MODAL_CONFIRM}     ${TIMEOUT}
    Page Should Contain Element    xpath=//*[@data-id='agendamento' and contains(text(),"${DATA} às 12:00")]
    Wait Until Element Is Visible  ${CONFIRMAR_ENVIO}   ${TIMEOUT}
    Sleep  2s  reason=Botão confirmar envio precisa de um tempo para se tornar clicável

Então deverá ser exibido os totais e a prévia na tela criar campanhas
    Wait Until Element Is Visible  ${BLOCO_TOTAIS}  ${TIMEOUT}
    Wait Until Element Is Visible  ${INFO_MESSAGE}  ${TIMEOUT}
    Execute Javascript    window.scrollTo(0,document.body.scrollHeight);

Então deverá ser exibido o modal de configuração de arquivo
    Wait Until Element Is Visible  id=model-config-template
    Wait Until Page Contains Element  xpath=//*[@id='select_numero_telefone']/option[@value='coluna0']  ${TIMEOUT}

Então deverá ser exibida a tela "${TELA}"
    Page Should Contain Element  xpath=//h5[contains(text(), "${TELA}")]
    Wait Until Element Is Visible  xpath=//*[@class='introjs-tooltip introjs-top-left-aligned']//a[contains(text(),'Sair')]  ${TIMEOUT}
    Click Element  xpath=//*[@class='introjs-tooltip introjs-top-left-aligned']//a[contains(text(),'Sair')]

Então deverá ser exibido uma informação sobre o produto
    Wait Until Element Is Visible  id=info-produto-container  ${TIMEOUT}

Então deverá ser exibido as informações da Empresa
    Wait Until Element Is Visible  id=info-empresa   ${TIMEOUT}

Então deverá ser exibido os números inválidos
    Wait Until Element Is Visible  xpath=//*[@id='bloco-totais']//div[contains(text(),'Inválidos')]//span[contains(text(),'2')]   ${TIMEOUT}

Então deverá ser exibido os números blacklist
    Wait Until Element Is Visible  xpath=//*[@id='bloco-totais']//div[contains(text(),'Blacklist')]//span[contains(text(),'1')]   ${TIMEOUT}