*** Settings ***
Resource                   ../Resource/Resource.robot
Suite SETUP                Abrir navegador
Suite TEARDOWN             Fechar navegador

*** Variables ***
#Locartos do formulário de Login
${CAMPO_EMAIL}            id=username
${CAMPO_SENHA}            id=password
${BOTAO_ENTRAR}           xpath=//*[@id='login_form']//button
${ESQUECI_MINHA_SENHA}    id=resetpass

#Locartos do formulário Esqueci minha senha
${CAMPO_EMAIL_RESET}      id=email_reset
${BOTAO_ENVIAR}           xpath=//*[@id='formulario_recovery']//button
${VOLTAR}                 id=resetback

#Variável auxiliar
${EMAIL_DESCONHECIDO}     guilhermeperesini@gmail.com

*** Test Case ***
Validação de login válido
    [tags] 
    Dado que estou na tela de login
    Quando preencher informações de login "interno"
    Então deve ser realizado o login no painel
    Realizar logout

# Validação de login com usuário externo
#     [tags]
#     Dado que estou na tela de login
#     Quando preencher informações de login "externo"
#     Então deve ser exibido o painel
#     Realizar logout

Validação de login sem preencher os dados
    [tags]    
    Dado que estou na tela de login
    Quando clicar em entrar sem informar "dados"
    Então deve ser exibida a mensagem de validação "Usuário e/ou senha inválidos."

Validação de login sem preencher e-mail
    [tags]      
    Dado que estou na tela de login
    Quando clicar em entrar sem informar "email"
    Então deve ser exibida a mensagem de validação "Usuário e/ou senha inválidos."

Validação de login sem preencher senha
    [tags] 
    Dado que estou na tela de login
    Quando clicar em entrar sem informar "senha"
    Então deve ser exibida a mensagem de validação "Usuário e/ou senha inválidos."
    
Validação de login com e-mail inválido
    [tags] 
    Dado que estou na tela de login
    Quando preencher o email com um "login inválido" e clicar em entrar
    Então deve ser exibida a mensagem de validação "Usuário e/ou senha inválidos."
    
Validação de login com senha inválida
    [tags]
    Dado que estou na tela de login
    Quando preencher a senha com um "valor inválido" e clicar em entrar
    Então deve ser exibida a mensagem de validação "Usuário e/ou senha inválidos."
    
Validação de recuperação de senha sem informar email
    [tags]
    Dado que estou na tela de login
    Quando clicar em "Esqueci minha senha"
    Então deve ser exibido o formulário de recuperação de senha

    Quando clicar em "Enviar"
    Então deve ser exibida a mensagem de validação "O login informado não é um e-mail, favor informar um e-mail"

Validação de recuperação de senha informando e-mail inválido
    [tags] 
    Dado que estou na tela de login
    Quando clicar em "Esqueci minha senha"
    Então deve ser exibido o formulário de recuperação de senha

    Quando preencher o email com um "login inválido" e clicar em enviar
    Então deve ser exibida a mensagem de validação "O login informado não é um e-mail, favor informar um e-mail"
    
Validação de recuperação de senha informando e-mail desconhecido
    [tags] 
    Dado que estou na tela de login
    Quando clicar em "Esqueci minha senha"
    Então deve ser exibido o formulário de recuperação de senha

    Quando preencher o email com um email desconhecido e clicar em enviar
    Então deve ser exibida a mensagem de validação "Usuário não existe, favor entrar em contato com o Administrador da sua empresa."

*** Keywords ***
Dado que estou na tela de login
    Go To  ${URL_SMS}

Dado que estou na tela de recuperação de senha
    Click Element   id=resetpass

Quando preencher informações de login "${TIPO}"
    Run Keyword If  '${TIPO}'=='interno'    Input Text      ${CAMPO_EMAIL}  ${LOGIN.username_beta}
    ...    ELSE IF  '${TIPO}'=='externo'    Input Text      ${CAMPO_EMAIL}  ${LOGIN.externo}
    Input Text       ${CAMPO_SENHA}         ${LOGIN.senha}
    Click Element    ${BOTAO_ENTRAR}

Quando clicar em entrar sem informar "${CAMPO}"
    Run Keyword If  '${CAMPO}'=='dados'     Click Element   ${BOTAO_ENTRAR}
    ...    ELSE IF  '${CAMPO}'=='email'     Run Keywords    Input Text          ${CAMPO_SENHA}  ${LOGIN.senha}      AND   Click Element   ${BOTAO_ENTRAR}
    ...    ELSE IF  '${CAMPO}'=='senha'     Run Keywords    Input Text          ${CAMPO_EMAIL}  ${LOGIN.username}   AND   Click Element   ${BOTAO_ENTRAR}


Quando preencher o email com um ${LOGIN_INVALIDO} e clicar em entrar
    Input Text      ${CAMPO_EMAIL}  ${LOGIN_INVALIDO}
    Input Text      ${CAMPO_SENHA}  ${LOGIN.senha}
    Click Element   ${BOTAO_ENTRAR}

Quando preencher a senha com um ${SENHA_INVALIDA} e clicar em entrar
    Input Text      ${CAMPO_EMAIL}  ${LOGIN.username}
    Input Text      ${CAMPO_SENHA}  ${SENHA_INVALIDA}
    Click Element   ${BOTAO_ENTRAR}

Quando clicar em "${ELEMENTO}"
    Run Keyword If  '${ELEMENTO}'=='Esqueci minha senha'    Click Element   ${ESQUECI_MINHA_SENHA}
    ...    ELSE IF  '${ELEMENTO}'=='Voltar'                 Click Element   ${VOLTAR}
    ...    ELSE IF  '${ELEMENTO}'=='Enviar'                 Click Element   ${BOTAO_ENVIAR}

Quando preencher o email com um ${LOGIN_INVALIDO} e clicar em enviar
    Input Text      ${CAMPO_EMAIL_RESET}    ${LOGIN_INVALIDO}
    Click Element   ${BOTAO_ENVIAR}

Quando preencher o email com um email desconhecido e clicar em enviar
    Input Text      ${CAMPO_EMAIL_RESET}    ${EMAIL_DESCONHECIDO}
    Click Element   ${BOTAO_ENVIAR}

Então deve ser realizado o login no painel
    Wait Until Element Is Visible   xpath=//*[@id='container']/h1[contains(text(),'Olá QA PGmais Homolog!')]	10s
    Page Should Contain             Selecione a empresa
    Selecionar empresa

Então deve ser exibido o painel
    Wait Until Page Contains    Campanhas em andamento
    Page Should Contain         Campanhas em andamento

Então deve ser exibida a mensagem de validação "${MSG_VALIDACAO}"
    Wait Until Element Is Visible   xpath=//span[contains(text(), '${MSG_VALIDACAO}')]  10s
    Page Should Contain             ${MSG_VALIDACAO}

Então deve ser exibido o formulário de recuperação de senha
    Wait Until Element Is Visible   xpath=//*[@id='login_recovery']/h1[@class='titulo-login']   10s
    Element Should Be Visible       id=login_recovery

Então deve ser exibido o formulário de login
    Element Should Be Visible   id=login_form