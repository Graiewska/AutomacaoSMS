*** Settings ***
Resource         ../Resource/Resource.robot
Suite SETUP       Abrir navegador
Suite TEARDOWN    Fechar navegador

*** Test Case ***
Validação de confirmação sem selecionar uma empresa
    [tags]    Regressao
    Dado que estou na tela de seleção de empresas
    Quando clicar em confirmar sem selecionar uma empresa
    Então deve ser exibida a mensagem de validação "Selecione uma empresa!"

Validação de seleção empresa para entrar no painel
    [tags]    Regressao
    Quando selecionar uma empresa e clicar em confirmar
    Então deve ser exibido o dashboard de campanhas

*** Keywords ***
Dado que estou na tela de seleção de empresas
    Realizar login      ${LOGIN.username_beta}       ${LOGIN.senha}

Quando clicar em confirmar sem selecionar uma empresa
    Click Element       ${BOTAO_CONFIRMAR}

Quando selecionar uma empresa e clicar em confirmar
    Selecionar empresa

Então deve ser exibida a mensagem de validação "${MSG_VALIDACAO}"
    Wait Until Element Is Visible   xpath=//span[contains(text(), '${MSG_VALIDACAO}')]    10 seconds
    Page Should Contain             ${MSG_VALIDACAO}

Então deve ser exibido o dashboard de campanhas
    Page Should Contain     Campanhas em andamento