*** Settings ***
Library               SeleniumLibrary
Library               String
Library               OperatingSystem
Library               FakerLibrary
Library               DateTime
Library               XML
Library               String
Library               Collections
 
*** Test Cases ***
teste de data
    ${data de hoje}=  Obter data atual
    Log To Console  esse é a data de hoje: ${data de hoje}

    ${data Acrescentada}=    Acrescentar dias à data atual  7
    Log To Console  esse é a data somada os dias: ${data Acrescentada}

*** Keywords ***
Obter data atual
    ${DATA}=          Get Current Date
    [Return]          ${DATA}

Acrescentar dias à data atual
    [Arguments]         ${DIAS}
    ${DATA}=            Obter data atual
    ${DATA}=            Add Time To Date    ${DATA}    ${DIAS} days
    ${DATA}=            Convert Date        ${DATA}    result_format=%d/%m/%Y
    [Return]            ${DATA}